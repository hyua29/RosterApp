package DAOtests;

import data.access.daos.PasswordResetTokenDAO;
import data.access.services.UserManagementService;
import data.access.tables.token.PasswordResetToken;
import data.access.tables.user.User;
import helper.Log;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration  // load the web application context
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/config/*"})
@Transactional
public class PasswordTokenDAOTest {

    @Autowired
    private PasswordResetTokenDAO passwordResetTokenDAO;

    @Autowired
    private UserManagementService userManagementService;

    private String existingToken;

    private int existingTokenId;

    @Before
    public void setUp() {

        User u = new User();
        u.setfName("first");
        u.setlName("last");
        u.setEmail("cooperhypvt@gmail.com");
        u.setPassword("ddd");
        u.setdOB(new Date());
        u.setState("active");
        u.setEmployType("C");
        userManagementService.addUser(u);

        PasswordResetToken t = passwordResetTokenDAO.createPasswordResetTokenForUser(u);
        existingTokenId = t.getUserId();
        existingToken = t.getToken();
    }

    @Test
    public void tokenToUserTest() {
        PasswordResetToken passwordResetToken = passwordResetTokenDAO.getTokenByUserId(existingTokenId);
        Assert.assertEquals("cooperhypvt@gmail.com", passwordResetToken.getUser().getEmail());
    }

    @Test
    public void resetExpiryTest() throws InterruptedException {
        PasswordResetToken passwordResetToken = passwordResetTokenDAO.getTokenByUserId(existingTokenId);
        Date originalExpiryDate = new Date(passwordResetToken.getExpiryDate().getTime());

        Thread.sleep(500);  // simulate time span
        passwordResetTokenDAO.resetExpiry(passwordResetToken);
        passwordResetToken = passwordResetTokenDAO.getTokenByUserId(existingTokenId);
        Assert.assertTrue(originalExpiryDate.before(passwordResetToken.getExpiryDate()));

    }

}
