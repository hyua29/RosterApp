package DAOtests;

import data.access.daos.UserDAO;
import data.access.tables.user.User;
import data.access.tables.userstoreauthority.UserStoreAuthority;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration  // load the web application context
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/config/*"})  // root web application at default path src/main/webapp
@Transactional
public class UserDAOTest {

    @Autowired
    UserDAO userDAO;


    @Autowired
    SessionFactory sessionFactory;
    @Test
    @Transactional
    public void testMapping() {
        Session session = sessionFactory.getCurrentSession();
        Query q = session.createQuery("FROM UserStoreAuthority");
        List<UserStoreAuthority> mappingList = q.getResultList();
        System.out.println(mappingList);
    }

    @Test
    @Transactional
    public void userAuthorityMappingTest() {
        User u =userDAO.getUserById(1);
        System.out.println(u.getUserStoreAuthorities().get(0).getId());
    }





}
