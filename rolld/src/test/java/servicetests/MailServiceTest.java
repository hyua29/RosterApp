package servicetests;


import data.access.services.NewUserInvitationService;
import data.access.services.ResetPasswordService;
import data.access.services.UserManagementService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import data.access.services.MailService;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration  // load the web application context
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/config/*"})
@Transactional
public class MailServiceTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Autowired
    MailService mailService;

    @Autowired
    UserManagementService userManagementService;

    @Autowired
    NewUserInvitationService newUserInvitationService;

    @Autowired
    ResetPasswordService resetPasswordService;

    @Before
    public void setUp() {

    }

   // @Test
    //public void notifyRosterReleaseTest() {
     //   mailService.notifyRosterRelease();
    //}

    @Test
    public void sendInvitationToUserTest() {  // FIXME: to pass this test, base url must match the local ip of the testing device
            //boolean succeeded = newUserInvitationService.sendInvitationToUser("http://192.168.0.5:8080", "cooperhypvt@gmail.com", "SASASAS");
            //Assert.assertTrue(succeeded);
    }

}
