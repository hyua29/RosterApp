package servicetests;

import data.access.daos.PasswordResetTokenDAO;
import data.access.services.ResetPasswordService;
import data.access.services.UserManagementService;
import data.access.tables.token.PasswordResetToken;
import data.access.tables.user.User;
import helper.Log;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration  // load the web application context
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/config/*"})
@Transactional
public class PasswordTokenServiceTest {
    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private ResetPasswordService resetPasswordService;

    private PasswordResetToken existingToken;

    @Before
    public void setup() throws Exception {
        System.out.println("setting up");
        existingToken = resetPasswordService.createPasswordResetTokenForUser("g@gmail.com");
    }

    @Test
    public void createPasswordTokenTest() {
        Date originalExpiryDate = existingToken.getExpiryDate();

        // expiry date should be renewed while token should remain the same
        PasswordResetToken newResetToken = resetPasswordService.createPasswordResetTokenForUser("g@gmail.com");
        Log.getLogger().info(newResetToken);
        Assert.assertTrue(originalExpiryDate.before(newResetToken.getExpiryDate()));
        Assert.assertEquals(existingToken.getToken(), newResetToken.getToken());

        String originalToken = existingToken.getToken();
        // set a expired token
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MINUTE, -61);
        existingToken.setExpiryDate(cal.getTime());
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(existingToken);
        System.out.println(existingToken.getExpiryDate());

        // expiry date should be renewed while token should remain the same
        newResetToken = resetPasswordService.createPasswordResetTokenForUser("g@gmail.com");
        System.out.println(existingToken.getToken());
        System.out.println(newResetToken.getToken());
        Assert.assertTrue(originalExpiryDate.before(newResetToken.getExpiryDate()));
        Assert.assertNotEquals(originalToken, newResetToken.getToken());


    }
}
