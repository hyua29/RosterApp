package servicetests;

import data.access.daos.UserStoreAuthorityDao;
import data.access.services.StoreManagementService;
import data.access.tables.store.AjaxRequestWorkRole;
import data.access.tables.store.WorkRole;
import data.access.tables.user.User;
import exceptions.ApplicationSpecificException;
import helper.Log;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration  // load the web application context
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/config/*"})  // root web application at default path src/main/webapp
@Transactional
public class StoreManagementServiceTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Autowired
    private UserStoreAuthorityDao userStoreAuthorityDao;

    @Autowired
    private StoreManagementService storeManagementService;


    @Test
    public void isManagerTest() {
        Assert.assertTrue(userStoreAuthorityDao.isManager(2, 2));

        Assert.assertFalse(userStoreAuthorityDao.isManager(1, 2));
        Assert.assertFalse(userStoreAuthorityDao.isManager(2, 1));
    }

    @Test
    public void isOwnerTest() {
        Assert.assertTrue(userStoreAuthorityDao.isManager(2, 2));

        Assert.assertFalse(userStoreAuthorityDao.isManager(1, 2));
        Assert.assertFalse(userStoreAuthorityDao.isManager(2, 1));
    }


    /**
     * purpose:
     * Test whether user name can be retrieved from db to a Map
     *
     * Assumption:
     * store 2 has user Gloria with email 'ooigloria@gmail.com'
     * store has 2 and only 2 users
     */
    @Test
    public void getAllEmployeeNamesTest() {

        Map<String, User> userMap1 = storeManagementService.getEmployeeDetailsFromCurrentStore("sname");
        Assert.assertTrue(userMap1.keySet().contains("ooigloria@gmail.com"));
        Assert.assertTrue(userMap1.size()==2);

        exception.expect(ApplicationSpecificException.class);
        Map<String, User> userMap2 = storeManagementService.getEmployeeDetailsFromCurrentStore("null store");
    }

    @Test
    public void saveWorkRoleTest() {
        AjaxRequestWorkRole requestWorkRole = new AjaxRequestWorkRole();
        requestWorkRole.setRoleName("Fryer");
        requestWorkRole.setRoleComment("Fry stuff(Testing)");
        requestWorkRole.setInTemplate(false);
        requestWorkRole.setStoreName("sname");

        WorkRole workRole = storeManagementService.createWorkRole(requestWorkRole);
        Assert.assertNotEquals(workRole, null);
    }

    @Test
    public void getWorkRoleTest() {
        List<WorkRole> workRoles = storeManagementService.getWorkRoleByStoreName("sname");
        Assert.assertTrue(workRoles.size()>=2);
    }

}
