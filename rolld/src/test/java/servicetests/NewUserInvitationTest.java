package servicetests;

import data.access.daos.UserDAO;
import data.access.services.NewUserInvitationService;
import data.access.tables.user.User;
import exceptions.ApplicationSpecificException;
import data.access.tables.user.UserDetailsByReceiver;
import data.access.tables.user.UserDetailsBySender;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration  // load the web application context
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/config/*"})  // root web application at default path src/main/webapp
@Transactional
public class NewUserInvitationTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private NewUserInvitationService newUserInvitationService;

    /**
     * Purpose
     * Test whether email can be send to the user
     * #################################
     *
     * Result:
     * passed; error
     */
    //@Test
    //public void sendInvitationToUserTest() {
      //  newUserInvitationService.sendInvitationToUser( "rolldgcr@gmail.com", "token");

        //exception.expect(ApplicationSpecificException.class);
        //newUserInvitationService.sendInvitationToUser( "rolldgcrgail.com", "token");  // not a valid email address
    //}

    /**
     * Purpose:
     * Test whether createTokenForNewUser can create invitation token with all the data required
     * and whether it returns null when input contains invalid data.
     *
     * ####################################################3
     *
     * Assumption:
     * STORE
     * store_name = sname
     *
     * USER_AUTHORITY
     * uesrauthority_name = EMPLOYEE
     *
     * ########################################
     *
     * Process:
     * 1. create user details
     * 2. check whether they can be successfully saved to the database
     * 3. check whether error can be thrown when store name does not exist
     *
     * #################################
     *
     * Result:
     * true; error
     *
     * ###################################
     */
    @Test
    public void createInvitationTokenForNewUserTest() {
        //UserDetailsByReceiver dbr = new UserDetailsByReceiver("new_fname", "new_lname", "password", new Date());
        UserDetailsBySender dbs = new UserDetailsBySender("rolldgcr@gmail.com", "P", "sname");
        Assert.assertTrue(newUserInvitationService.createTokenForNewUser(dbs) != null);

        UserDetailsBySender dbsn = new UserDetailsBySender("rolldgcr@gmail.com", "P", "null");  // store name isn't valid
        exception.expect(ApplicationSpecificException.class);
        newUserInvitationService.createTokenForNewUser(dbsn);

//        UserDetailsBySender dbsnn = new UserDetailsBySender("rolldgcr@gmail.com", "P", "sname"); // authority ame isn't valid
//        Assert.assertTrue(newUserInvitationService.createTokenForNewUser(dbsnn) == null);
    }

    /**
     * Purpose:
     * Test whether token can be verified
     *
     * ####################################################3
     *
     * Assumption:
     *
     * INVITATION_TOKEN
     * invitation_token = qazwsx
     *
     * #################################
     *
     * Result:
     * true; false
     *
     * ###################################
     */
    @Test
    public void verifyTokenTest() {
        Assert.assertTrue(newUserInvitationService.verifyResetToken("qazwsx"));
        Assert.assertFalse(newUserInvitationService.verifyResetToken("null"));
    }


    /**
     * Purpose:
     * test new user can be created with the token and the details provided by the new user on the sign up page
     *
     * ####################################
     *
     * Assumption:
     * INVITATION_TOKEN
     * invitation_token = 82f15c94-0fbf-4b62-8924-8b84472e7621
     * expire_date = 2018-07-07 21:33:27
     *
     * invitation_token = 82f15c94-0fbf-4b62-8924-8b84472e7621
     * expire_date = 2018-07-07 21:33:27
     *
     * ########################################
     *
     * Process:
     * 1. create dummy user sign up data
     * 2. create user account with an expired token
     * 2. create user account with a valid token
     *
     * #######################################
     *
     * Result:
     * failed; passed
     *
     * ###################################
     */

    @Test
    public void createNewUserTest() {
        UserDetailsByReceiver userDetailsByReceiver1 = new UserDetailsByReceiver("haoming", "yuan", "1234", "1234", new Date());

        // token has expired
        exception.expect(ApplicationSpecificException.class);
        newUserInvitationService.createNewUser(userDetailsByReceiver1, "82f15c94-0fbf-4b62-8924-8b84472e7621");

        // a valid token
        newUserInvitationService.createNewUser(userDetailsByReceiver1, "qaz");
        User user = userDAO.getUserByEmail("gg@gmail.com");
        Assert.assertEquals(user.getfName(), "haoming");

    }
}
