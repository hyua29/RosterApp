package servicetests;

import data.access.daos.UserAuthorityDAO;
import data.access.services.UserManagementService;
import data.access.tables.user.User;
import data.access.tables.authority.UserAuthority;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration  // load the web application context
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/config/*"})  // root web application at default path src/main/webapp
@Transactional
public class UserServiceTest {

    @Autowired
    UserManagementService userManagementService;

    @Autowired
    UserAuthorityDAO userAuthorityDAO;

    @Test
    public void getUserByEmailTest() {
        User u = userManagementService.getUserByEmail("rolldgcr@gmail.com");
        Assert.assertEquals("fname", u.getfName());
    }


}
