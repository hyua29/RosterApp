package DBInitialisationTest;

import data.access.daos.UserDAO;
import data.access.tables.user.Availability;
import data.access.tables.user.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

import static org.hamcrest.core.IsNull.nullValue;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration  // load the web application context
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/config/*"})
@Transactional
public class AvailabilityTableTest {
    Time timeT;

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private UserDAO userDAO;
//    @Autowired
//    private AvailabilityDAO availabilityDAO;

    private Availability createAvailabilityObj(){
        return new Availability("FRI" , "00:00:23","00:30:11");
    }

    private User createUserObj(){
        User u = new User("a", "b", new Date(1), "s", "a", "a", "");
        userDAO.addUser(u);
        return u;
    }

    @Test
    public void insertAvailability(){
        Session session = sessionFactory.getCurrentSession();

        Availability availability = createAvailabilityObj();
        availability.setUser(createUserObj());
        Serializable s = session.save(availability);
        Assert.assertEquals("FRI",session.get(Availability.class, s).getAvailableDay());
        Assert.assertNotEquals("invalid day",session.get(Availability.class, s).getAvailableDay());
    }

    @Test
    public void deleteAvailability(){
        Session session = sessionFactory.getCurrentSession();
        session.flush();
        Availability availability = createAvailabilityObj();
        availability.setUser(createUserObj());
        Serializable s = session.save(availability);
        Assert.assertEquals(availability, session.find(Availability.class, availability.getAvailabilityId()));
        session.remove(availability);
        Assert.assertThat(session.find(Availability.class, availability.getAvailabilityId()),nullValue());
    }

    @Test
    public void updateAvailability(){
        Session session = sessionFactory.getCurrentSession();
        session.flush();
        Availability availability = createAvailabilityObj();
        availability.setUser(createUserObj());
        Serializable s = session.save(availability);
        availability.setAvailableDay("WED");
        session.update(availability);
        Assert.assertEquals("WED",session.get(Availability.class, s).getAvailableDay());
        Assert.assertNotEquals("FRI",session.get(Availability.class, s).getAvailableDay());
    }


}
