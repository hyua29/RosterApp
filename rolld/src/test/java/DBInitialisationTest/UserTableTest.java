package DBInitialisationTest;

import data.access.daos.UserDAO;
import data.access.tables.TableUtil;
import data.access.tables.user.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Date;

import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration  // load the web application context
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/config/*"})

public class UserTableTest {


    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private UserDAO userDAO;



    private User createUserObj(){
        Date dob = TableUtil.parseDobIntoDate("16-04-1996");
        User user = new User("Gloria","Ooi", dob , "fabulousglors@gmail.com","ABC", "PT", "ACTIVE");
        return user;
    }


    @Test
    @Transactional
    public void inserUser(){
        Session session = sessionFactory.getCurrentSession();
        session.flush();
        User user = createUserObj();
        Serializable s = session.save(user);
        Assert.assertEquals("fabulousglors@gmail.com",session.get(User.class, s).getEmail());
        Assert.assertNotEquals("invalid@invalid.com",session.get(User.class, s).getEmail());
    }

    @Test
    @Transactional
    public void deleteUser(){
        Session session = sessionFactory.getCurrentSession();
        session.flush();
        User user = createUserObj();
        Serializable s = session.save(user);
        Assert.assertEquals(user, session.find(User.class, user.getId()));
        session.remove(user);
        Assert.assertThat(session.find(User.class, user.getId()),nullValue());
    }

    @Test
    @Transactional
    public void updateUser(){
        Session session = sessionFactory.getCurrentSession();
        session.flush();
        User user = createUserObj();
        Serializable s = session.save(user);
        user.setEmployType("FT");
        session.update(user);
        Assert.assertEquals("FT",session.get(User.class, s).getEmployType());
        Assert.assertNotEquals("PT",session.get(User.class, s).getEmployType());
    }



    /**
     * Purpose:     Testing inserting user with valid data
     *
     */
    @Test
    @Transactional
    public void insertUser1(){
        User user = createUserObj();
        userDAO.addUser(user);
    }



    /**
     * Testing getUserById
     *
     * Assumption: there is a user by id 1.
     * Expected result: a user object should be returned
     */
    @Test
    @Transactional
    public void getUserByIdTest(){
        User u = userDAO.getUserById(1);
        System.out.println(u.toString());
        assertNotNull(u);
    }


    @Test
    @Transactional
    public void updateUserTest(){
        User u = createUserObj();
        userDAO.addUser(u);
        System.out.println(u.toString());
        u.setEmployType("LLLLL");

        userDAO.updateUserState(u);
        System.out.println(u.toString());
    }

    @Transactional
    @Test
    public void getAvailabilityTest() {
        User user = userDAO.getUserByEmail("ooigloria@gmail.com");
        System.out.println(user.getAvailability());
    }
}
