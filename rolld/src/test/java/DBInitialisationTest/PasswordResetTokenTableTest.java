package DBInitialisationTest;

import data.access.daos.UserDAO;
import data.access.tables.token.PasswordResetToken;
import data.access.tables.TableUtil;
import data.access.tables.user.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Date;

import static org.hamcrest.core.IsNull.nullValue;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration  // load the web application context
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/config/*"})
public class PasswordResetTokenTableTest {
    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private UserDAO userDAO;

    private PasswordResetToken createResetTokenObj(){
        Date dob = TableUtil.parseDobIntoDate("16-04-1996");
        User user = new User("Gloria","Ooi", dob , "fabulousglors@gmail.com","ABC", "PT", "ACTIVE");
        user.setId(12345);
        userDAO.addUser(user);
        PasswordResetToken passwordResetToken = new PasswordResetToken(user);  // FIXME: I've changed the constructor
        return passwordResetToken;
    }

    @Test
    @Transactional
    public void insertToken(){
        Session session = sessionFactory.getCurrentSession();
        session.flush();
        PasswordResetToken passwordResetToken = createResetTokenObj();
        Serializable s = session.save(passwordResetToken);
        Assert.assertEquals(12345,session.get(PasswordResetToken.class, s).getUserId());
        Assert.assertNotEquals(00000,session.get(PasswordResetToken.class, s).getUserId());
    }

    @Test
    @Transactional
    public void deleteToken(){
        Session session = sessionFactory.getCurrentSession();
        session.flush();
        PasswordResetToken passwordResetToken = createResetTokenObj();
        Serializable s = session.save(passwordResetToken);
        Assert.assertEquals(passwordResetToken, session.find(PasswordResetToken.class, passwordResetToken.getUserId()));
        session.remove(passwordResetToken);
        Assert.assertThat(session.find(PasswordResetToken.class, passwordResetToken.getUserId()),nullValue());
    }

    @Test
    @Transactional
    public void updateToken(){
        Session session = sessionFactory.getCurrentSession();
        session.flush();
        PasswordResetToken passwordResetToken = createResetTokenObj();
        Serializable s = session.save(passwordResetToken);
        passwordResetToken.setUserId(00000);
        session.update(passwordResetToken);
        Assert.assertEquals(00000,session.get(PasswordResetToken.class, s).getUserId());
        Assert.assertNotEquals(12345,session.get(PasswordResetToken.class, s).getUserId());
    }
}
