package DBInitialisationTest;

import data.access.daos.InvitationTokenDAO;
import data.access.tables.token.InvitationToken;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

import static org.hamcrest.core.IsNull.nullValue;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration  // load the web application context
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/config/*"})
public class InvitationTokenTableTest {
    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private InvitationTokenDAO invitationTokenDAO;

   private InvitationToken createInvitationToken(){
       InvitationToken invitationToken = new InvitationToken("janzen0821lim@gmail.com", 1, 1, "PT");
       return invitationToken;
   }

    @Test
    @Transactional
    public void insertToken(){
        Session session = sessionFactory.getCurrentSession();
        session.flush();
        InvitationToken invitationToken = createInvitationToken();
        Serializable s = session.save(invitationToken);
        Assert.assertEquals("janzen0821lim@gmail.com",session.get(InvitationToken.class, s).getInviteEmail());
        Assert.assertNotEquals("invalid@invalid.com",session.get(InvitationToken.class, s).getInviteEmail());
    }

    @Test
    @Transactional
    public void deleteToken(){
        Session session = sessionFactory.getCurrentSession();
        session.flush();
        InvitationToken invitationToken = createInvitationToken();
        Serializable s = session.save(invitationToken);
        Assert.assertEquals(invitationToken, session.find(InvitationToken.class, invitationToken.getInvitationToken()));
        session.remove(invitationToken);
        Assert.assertThat(session.find(InvitationToken.class, invitationToken.getInvitationToken()),nullValue());
    }

    @Test
    @Transactional
    public void updateToken(){
        Session session = sessionFactory.getCurrentSession();
        session.flush();
        InvitationToken invitationToken = createInvitationToken();
        Serializable s = session.save(invitationToken);
        invitationToken.setEmpType("FT");
        session.update(invitationToken);
        Assert.assertEquals("FT",session.get(InvitationToken.class, s).getEmpType());
        Assert.assertNotEquals("PT",session.get(InvitationToken.class, s).getEmpType());
    }

//DAO Tests
    //======================================================================================
    @Test
    @Transactional
    public void saveToken(){
        InvitationToken token = createInvitationToken();
        invitationTokenDAO.saveInvitationToken(token);
    }

    @Test
    @Transactional
    public void getToken(){
        InvitationToken token = createInvitationToken();
        invitationTokenDAO.saveInvitationToken(token);
        Assert.assertEquals(token,invitationTokenDAO.getTokenByToken(token.getInvitationToken()));
    }


}
