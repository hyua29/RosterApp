package DBInitialisationTest;


import data.access.tables.store.Roster;
import data.access.tables.store.Shift;
import data.access.tables.store.Store;
import data.access.tables.TableUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.core.IsNull.nullValue;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration  // load the web application context
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/config/*"})
public class RosterTableTest {
    @Autowired
    private SessionFactory sessionFactory;

    private Roster createRosterObj(Session session){
        Date date = TableUtil.parseDobIntoDate("20-07-2018");
        Store store = new Store("rolld","Monash Road", "Clayton",3168, "Australia");
        session.save(store);
        List<Shift> shiftList = new ArrayList<Shift>();
        return new Roster(store.getStoreId(),date,store,shiftList);
    }

    @Test
    @Transactional
    public void insertRoster(){
        Session session = sessionFactory.getCurrentSession();
        session.flush();
        Roster roster = createRosterObj(session);
        Serializable s = session.save(roster);
        Assert.assertEquals("rolld",session.get(Roster.class, s).getStore().getStoreName());
        Assert.assertNotEquals("mcdonalds",session.get(Roster.class, s).getStore().getStoreName());
    }

    @Test
    @Transactional
    public void deleteRoster(){
        Session session = sessionFactory.getCurrentSession();
        session.flush();
        Roster roster = createRosterObj(session);
        Serializable s = session.save(roster);
        Assert.assertEquals(roster, session.find(Roster.class, roster.getRosterId()));
        session.remove(roster);
        Assert.assertThat(session.find(Roster.class, roster.getRosterId()),nullValue());
    }

    @Test
    @Transactional
    public void updateRoster(){
        Session session = sessionFactory.getCurrentSession();
        session.flush();
        Roster roster = createRosterObj(session);
        Serializable s = session.save(roster);
        Date dateValid = TableUtil.parseDobIntoDate("22-07-2018");
        Date dateInvalid = TableUtil.parseDobIntoDate("20-07-2018");
        roster.setRosterWeekStartDate(dateValid);
        session.update(roster);
        Assert.assertEquals(dateValid,session.get(Roster.class, s).getRosterWeekStartDate());
        Assert.assertNotEquals(dateInvalid,session.get(Roster.class, s).getRosterWeekStartDate());
    }
}
