package DBInitialisationTest;

import data.access.tables.store.Store;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

import static org.hamcrest.core.IsNull.nullValue;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration  // load the web application context
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/config/*"})
public class StoreTableTest {
    @Autowired
    private SessionFactory sessionFactory;

    private Store createStoreObj(){
        return new Store("rolld","Monash Road", "Clayton",3168, "Australia");
    }

    @Test
    @Transactional
    public void inserStore(){
        Session session = sessionFactory.getCurrentSession();
        session.flush();
        Store store = createStoreObj();
        Serializable s = session.save(store);
        Assert.assertEquals("rolld",session.get(Store.class, s).getStoreName());
        Assert.assertNotEquals("maccas",session.get(Store.class, s).getStoreName());
    }

    @Test
    @Transactional
    public void deleteStore(){
        Session session = sessionFactory.getCurrentSession();
        session.flush();
        Store store = createStoreObj();
        Serializable s = session.save(store);
        Assert.assertEquals(store, session.find(Store.class, store.getStoreId()));
        session.remove(store);
        Assert.assertThat(session.find(Store.class, store.getStoreId()),nullValue());
    }

    @Test
    @Transactional
    public void updatStore(){
        Session session = sessionFactory.getCurrentSession();
        session.flush();
        Store store = createStoreObj();
        Serializable s = session.save(store);
        store.setStoreCountry("Malaysia");
        session.update(store);
        Assert.assertEquals("Malaysia",session.get(Store.class, s).getStoreCountry());
        Assert.assertNotEquals("Australia",session.get(Store.class, s).getStoreCountry());
    }

}
