import data.access.daos.*;
import data.access.services.NewUserInvitationService;
import data.access.services.UserManagementService;
import data.access.tables.authority.UserAuthority;
import data.access.tables.store.Store;
import data.access.tables.token.InvitationToken;
import data.access.tables.token.PasswordResetToken;
import data.access.tables.user.User;
import data.access.tables.userstoreauthority.UserStoreAuthority;
import exceptions.ApplicationSpecificException;
import helper.Log;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration  // load the web application context
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/config/*"})  // root web application at default path src/main/webapp
@Transactional
public class BasicMappingTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private PasswordResetTokenDAO passwordResetTokenDAO;

    @Autowired
    private InvitationTokenDAO invitationTokenDAO;

    @Autowired
    private UserManagementService userManagementService;

    @Autowired
    private UserAuthorityDAO userAuthorityDAO;

    @Autowired
    private StoreDAO storeDAO;

    @Autowired
    private NewUserInvitationService newUserInvitationService;

    /**
     * * Purpose
     * -----------
     * Test whether 'USER' table can be recognized by the mapping class
     *
     * ########################################
     *
     * Test Assumption
     * ---------------------
     * Table: USER
     * Value: id = 2; user_fname = fname
     *
     * ##########################################
     *
     * Test Process
     * ---------------
     * 1. get user by given id(2)
     * 2. check whether it matches the expected value(fname)
     * 3. check whether a null value is returned as a result of invalid token(null)
     *
     * ###########################################
     *
     * Test Result
     * -----------
     * user_fname = fname
     *
     * ############################################
     */
    @Test
    public void UserMappingTest() {
        User u = userDAO.getUserById(2);
        Assert.assertEquals(u.getfName(), "fname");

        exception.expect(ApplicationSpecificException.class);
        User t = userDAO.getUserById(-1);

    }

    /**
     * * Purpose
     * -----------
     * Test whether 'STORE' table can be recognized by the mapping class
     *
     * ########################################
     *
     * Test Assumption
     * ---------------------
     * Table: STORE
     * Value: id = 2; store_name = sname
     *
     * ##########################################
     *
     * Test Process
     * ---------------
     * 1. get user by given id(2)
     * 2. check whether it matches the expected value(sname)
     * 3. check whether a null value is returned as a result of invalid token(null)
     *
     * ###########################################
     *
     * Test Result
     * -----------
     * store_name = sname
     *
     * ############################################
     */
    @Test
    public void storeMappingTest() {
        Store s = storeDAO.getStoreById(2);
        Assert.assertEquals(s.getStoreName(), "sname");


        Store sn = storeDAO.getStoreById(-1);
        Assert.assertTrue(sn == null);
    }

    /**
     * * Purpose
     * -----------
     * Test whether user authority can be retrieved from 'USER' table can be recognized by the mapping class
     *
     * ########################################
     *
     * Test Assumption
     * ---------------------
     * Table: USER
     * Value: id = 2;
     *
     * Table: USER_STORE_AUTHORITY
     * Value: user_id = 2; store_id = 2; authority_id = 2
     * Value: user_id = 2; store_id = 2; authority_id = 1
     *
     * ##########################################
     *
     * Test Process
     * ---------------
     * 1. get user by given id(2)
     * 2. check whether it matches the expected user_lname (sname)
     * 3. check whether this user has manager and employee roles
     *
     * ###########################################
     *
     * Test Result
     * -----------
     * userauthority_name = EMPLOYEE and userauthority_name = MANAGER and  userauthority_name = OWNER
     *
     * ############################################
     */
    @Test
    public void getUserAuthorityFromUserTest() {
        User u = userDAO.getUserById(2);
        Assert.assertEquals(u.getfName(), "fname");

        Assert.assertTrue(u.getAuthorityList().size() == 3);  // this user three roles

        boolean isEmployee = false;
        boolean isManager = false;
        for(UserAuthority a : u.getAuthorityList()) {
            if(a.getAuthority().equals("ROLE_EMPLOYEE"))
                isEmployee = true;
            if(a.getAuthority().equals("ROLE_MANAGER"))
                isManager = true;
        }

        Assert.assertTrue(isEmployee && isManager);  // this user is the manager and the employee
    }

    /**
     * Purpose
     * -----------
     * Test whether 'INVITATION_TOKEN' table can be recognized by the mapping class
     *
     * ########################################
     *
     * Test Assumption
     * ---------------------
     * Table: INVITATION_TOKEN
     * Value: invitation_token = qazwsx; invite_email = rollgcr@gmail.com;
     *
     * Table: STORE
     * store_name = sname; store_id = 2
     *
     *
     * ##########################################
     *
     * Test Process
     * ---------------
     * 1. get invitation token by given id(qazwsx)
     * 2. check whether it matches the expected invite_email (rollgcr@gmail.com)
     * 3. check whether the store can be retrieved from the STORE table
     * 4. check whether a null value is returned as a result of invalid token(null)
     *
     * ###########################################
     *
     * Test Result
     * -----------
     * invite_email = rollgcr@gmail.com
     * store_name = sname
     * store_id = 2
     *
     * ############################################
     */
    @Test
    public void invitationTokenMappingTest() {
        InvitationToken i = invitationTokenDAO.getTokenByToken("qazwsx");
        Assert.assertEquals(i.getInviteEmail(), "rollgcr@gmail.com");
        Assert.assertEquals(i.getStoreId(), 2);
        Assert.assertEquals(i.getStore().getStoreName(), "sname");

        exception.expect(ApplicationSpecificException.class);
        InvitationToken in = invitationTokenDAO.getTokenByToken("null");
    }



    @Test
    public void UAMappingTest() {
        UserAuthority uA = userAuthorityDAO.getUserAuthorityByName("MANAGER");
        Assert.assertEquals(uA.getAuthority(), "ROLE_MANAGER");
    }

    @Test
    public void UserStoreAuthorityMappingTest() {
        Session session = sessionFactory.getCurrentSession();
        Query q = session.createQuery("FROM UserStoreAuthority");
        List<UserStoreAuthority> mappingList = q.getResultList();
        System.out.println(mappingList);
    }

    @Test
    public void userAuthorityMappingTest() {
        User u =userDAO.getUserById(1);
        System.out.println(u.getUserStoreAuthorities().get(0).getId());

    }

    @Test
    public void userMappingTest() {
        User u = new User();
        u.setfName("first");
        u.setlName("last");
        u.setEmail("cooperhypvt@gmail.com");
        u.setPassword("ddd");
        u.setdOB(new Date());
        u.setState("active");
        //u.set(userAuthorities);
        u.setEmployType("C");
        userDAO.addUser(u);
    }

    @Test
    public void passwordTokenMappingTest() {
        User u = new User();
        u.setfName("first");
        u.setlName("last");
        u.setEmail("cooperhypvt@gmail.com");
        u.setPassword("ddd");
        u.setdOB(new Date());
        u.setState("active");
        u.setEmployType("C");
        userManagementService.addUser(u);

        PasswordResetToken t = passwordResetTokenDAO.createPasswordResetTokenForUser(u);
    }

//    @Test FIXME: rosterMappingTest gets 0 shifts instead of 2 though there are 2 shifts in the chosen roster
//    public void rosterMappingTest() {
//        Session session = sessionFactory.getCurrentSession();
//        Roster r = session.get(Roster.class, 1);
//        System.out.println(r);
//        System.out.println(r.getRosterId());
//        System.out.println(r.getShiftList().size());
//        System.out.println(r.getShiftList().get(0).getShiftDay());
//        Assert.assertTrue(r.getShiftList().size() > 0);
//    }

    @Test
    public void invitationTokenMapping() {
        Session session = sessionFactory.getCurrentSession();
        InvitationToken i = session.get(InvitationToken.class, "qaz");
        Log.getLogger().info(i.getExpiryDate());
        //Assert.assertTrue(newUserInvitationService.createTokenForNewUser("a@gmail.com", "rolld", "EMPLOYEE", "C" ) != null);
    }

    /**
     * Purpose
     * -----------
     * Test whether work role can be retrieved by store
     *
     * ########################################
     *
     * Test Assumption
     * ---------------------
     * Table: STORE
     * Value: store_id = 2; store_name = sname;
     *
     * Table: WORK_ROLE
     * role_name = test_role; store_id = 2
     *
     *
     * ##########################################
     *
     * Test Process
     * ---------------
     * 1. get store by store name
     * 2. get work role from store
     * 3. check the work role name
     *
     * ###########################################
     *
     * Test Result
     * -----------
     * role_name = test_role
     *
     * ############################################
     */
    @Test
    public void getWorkRoleFromStore() {
        Store s = storeDAO.getStoreByStoreName("sname");
        Assert.assertEquals(s.getWorkRoles().get(0).getRoleName(), "test_role");
    }


}
