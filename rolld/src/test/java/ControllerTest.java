import data.access.services.ResetPasswordService;
import data.access.tables.authority.UserAuthority;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.http.server.reactive.MockServerHttpRequest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.mock.web.MockServletContext;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import spring.config.routing.AuthenticationController;
import spring.config.routing.MainPageController;

import javax.servlet.ServletContext;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.mock.http.server.reactive.MockServerHttpRequest.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @Mock creates a mock. @InjectMocks creates an instance of the class and injects the mocks that are created with the @Mock (or @Spy) annotations into this instance
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration  // load the web application context
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/config/*", "file:src/main/webapp/WEB-INF/spring/servlets/*"})  // root web application at default path src/main/webapp
@Transactional
public class ControllerTest {

    private MockMvc mockMvc;

    @Autowired
    WebApplicationContext webApplicationContext;

    public static class MockSecurityContext implements SecurityContext {

        private Authentication authentication;

        public MockSecurityContext(Authentication authentication) {
            this.authentication = authentication;
        }

        public Authentication getAuthentication() {
            return this.authentication;
        }

        public void setAuthentication(Authentication authentication) {
            this.authentication = authentication;
        }
    }


    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                //.addFilters(webConfigurer.corsFilter())
                .build();
    }

    @Test
    public void givenWac_whenServletContext_thenItProvidesGreetController() {
        ServletContext servletContext = webApplicationContext.getServletContext();

        Assert.assertNotNull(servletContext);
        Assert.assertTrue(servletContext instanceof MockServletContext);
        Assert.assertNotNull(webApplicationContext.getBean(AuthenticationController.class));
    }

    @Test
    @WithMockUser(username = "admin", roles = { "MANAGER", "EMPLOYEE"})
    public void accessTest() throws Exception {
        HttpSessionCsrfTokenRepository httpSessionCsrfTokenRepository = new HttpSessionCsrfTokenRepository();
        CsrfToken csrfToken = httpSessionCsrfTokenRepository.generateToken(new MockHttpServletRequest());


//        UsernamePasswordAuthenticationToken principal =  new UsernamePasswordAuthenticationToken("name","pw", new ArrayList<UserAuthority>());
//        MockHttpSession session = new MockHttpSession();
//        session.setAttribute(
//                HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY,
//                new MockSecurityContext(principal));

        mockMvc.perform(MockMvcRequestBuilders.get("/home").param("name", "dsd"))
                .andDo(print())
                .andExpect(view().name("mySchedule"))
                .andExpect((status().isOk()));
//        mockMvc.perform(MockMvcRequestBuilders.post("/authentication/send-password-token")
//        .param("email", "dasdasd").sessionAttr("_csrf", csrfToken)).andDo(print());
    }

//    private String day;
//    private String availabilityStartTime;
//    private  String getAvailabilityEndTime;
//    @Test
//    @WithMockUser(username = "admin", roles = { "MANAGER"})
//    public void getEmployeesTest() throws Exception {
//        mockMvc.perform(MockMvcRequestBuilders.post("/home/availabilities/availability-save").accept(APPLICATION_JSON)
//                .param("day", "day").param("availabilityStartTime", "availabilityStartTime").param("getAvailabilityEndTime", "getAvailabilityEndTime"))
//                .andDo(print())
//              //  .andExpect(content().contentType("application/json"))
//                .andExpect((status().isOk()));
//               // .andExpect(jsonPath("$.fname").value("Lee"));
//
//    }



}
