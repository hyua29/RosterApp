package data.access.daos;

import data.access.tables.user.User;
import data.access.tables.userstoreauthority.UserStoreAuthority;
import exceptions.ExceptionFactory;
import exceptions.ExceptionType;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

import static data.access.tables.authority.AuthorityType.MANAGER;
import static data.access.tables.authority.AuthorityType.OWNER;


@Repository
public class UserStoreAuthorityDao {

    @Autowired
    private SessionFactory sessionFactory;

    public boolean isManager(int userId, int storeId) {
        Session session = sessionFactory.getCurrentSession();
        Query q = session.createQuery("FROM UserStoreAuthority WHERE user_id=:userId AND store_id=:storeId")
                .setParameter("userId", userId)
                .setParameter("storeId", storeId);
        List<UserStoreAuthority> matchingUsa= (List<UserStoreAuthority>) q.getResultList();
        if(matchingUsa==null)
            throw ExceptionFactory.create(new ValueException("Invalid user id or store id"), ExceptionType.HIBERNATE_SEARCH_ERROR);
        for(UserStoreAuthority usa : (List<UserStoreAuthority>) q.getResultList()) {
            if(usa.getUserAuthority().getAuthority().equals(MANAGER.getMask()))
                return true;
        }
        return false;
    }

    public boolean isOwner(int userId, int storeId) {
        Session session = sessionFactory.getCurrentSession();
        Query q = session.createQuery("FROM UserStoreAuthority WHERE user_id=:userId AND store_id=:storeId")
                .setParameter("userId", userId)
                .setParameter("storeId", storeId);
        List<UserStoreAuthority> matchingUsa= (List<UserStoreAuthority>) q.getResultList();
        if(matchingUsa==null)
            throw ExceptionFactory.create(new ValueException("Invalid user id or store id"), ExceptionType.HIBERNATE_SEARCH_ERROR);
        for(UserStoreAuthority usa : (List<UserStoreAuthority>) q.getResultList()) {
            if(usa.getUserAuthority().getAuthority().equals(OWNER.getMask()))
                return true;
        }
        return false;
    }

    /**
     * Get all the employees/manager/owner who belong to the store
     * @param storeId
     * @return
     */
    //TODO: might need to exclude the manager and owner
    public List<User> getEmployeesFromStore(int storeId, boolean withAvailabilities) {
        Session session = sessionFactory.getCurrentSession();
        Query q = session.createQuery("FROM UserStoreAuthority WHERE store_id=:storeId")
                .setParameter("storeId", storeId);
        List<UserStoreAuthority> matchingUsa= (List<UserStoreAuthority>) q.getResultList();
        if(matchingUsa==null)
            throw ExceptionFactory.create(new ValueException("Invalid store id"), ExceptionType.HIBERNATE_SEARCH_ERROR);

        List<User> employees = new ArrayList<User>();
        for(UserStoreAuthority usa : matchingUsa) {
            if (!employees.contains(usa.getUser())) {
                if(withAvailabilities)
                    Hibernate.initialize(usa.getUser().getAvailability()); // force hibernate to load the availability(fetch type =  lazy)
                employees.add(usa.getUser()); //TODO: might need to exclude the manager and owner
            }
        }
        return employees;
    }

    /**
     *
     * @param managerOrOwnerId
     * @return All the stores the manager/owner is in charge of; empty if the given id isn't valid or it is an employee id
     */
    public List<String> getStoreNames(int managerOrOwnerId) {

        Session session = sessionFactory.getCurrentSession();
        Query q = session.createQuery("FROM UserStoreAuthority WHERE user_id=:userId")
                .setParameter("userId", managerOrOwnerId);
        List<UserStoreAuthority> matchingUsa= (List<UserStoreAuthority>) q.getResultList();
        if(matchingUsa==null)
            throw ExceptionFactory.create(new ValueException("Invalid user id"), ExceptionType.HIBERNATE_SEARCH_ERROR);

        List<String> storeNames = new ArrayList<String>();
        for(UserStoreAuthority usa : matchingUsa) {
            if (!storeNames.contains(usa.getStore().getStoreName())
                    && (usa.getUserAuthority().getAuthority().equals(MANAGER.getMask()) || usa.getUserAuthority().getAuthority().equals(OWNER.getMask())))
                storeNames.add(usa.getStore().getStoreName()); //TODO: might need to exclude the manager and owner
        }
        return storeNames;
    }
}
