package data.access.daos;

import data.access.tables.authority.UserAuthority;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserAuthorityDAO {
    @Autowired
    SessionFactory sessionFactory;

    public UserAuthority getUserAuthorityByName(String authorityName) {
        Session session = sessionFactory.getCurrentSession();
        Query q = session.createQuery("from UserAuthority where userauthority_name=:authority");
        q.setParameter("authority", authorityName.toUpperCase());

        if (q.getResultList().size() > 1)
            throw new ValueException("Authority name should be unique");

        if (q.getResultList().size() < 1)
            throw new ValueException("Wrong authority name");


        return (UserAuthority) q.getResultList().get(0);  // there should only be one user
    }

    public int getUserAuthorityIdByName(String authorityName) {
        return getUserAuthorityByName(authorityName).getUserAuthorityId();  // there should only be one user
    }
}
