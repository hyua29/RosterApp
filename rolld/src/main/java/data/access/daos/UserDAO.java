package data.access.daos;

import data.access.tables.user.User;
import exceptions.ExceptionFactory;
import helper.Log;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static exceptions.ExceptionType.HIBERNATE_SEARCH_ERROR;
import static exceptions.ExceptionType.HIBERNATE_UPDATE_ERROR;
import static exceptions.ExceptionType.HTTP_INTERNAL_SERVER_ERROR;

@Repository
public class UserDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public User getUserById(int id) {
        Session session = sessionFactory.getCurrentSession();
        User u = session.get(User.class, id);
        if(u == null)
            throw ExceptionFactory.create(HIBERNATE_SEARCH_ERROR);
        return u;
    }

    public List<User> getAllUsers() {
        Session session = sessionFactory.getCurrentSession();
        Query q = session.createQuery("FROM User");
        List<User> userList = q.getResultList();
        return userList;
    }

    /**
     * return t he user by email
     * @param email
     * @return return the user with the given email or null if no user has such an email
     */

    public User getUserByEmail(String email) {
        Session session = sessionFactory.getCurrentSession();
        Query q = session.createQuery("FROM User WHERE user_email=:email").setParameter("email", email);

        User u;
        try {
            u = (User) q.getSingleResult();
        } catch (Exception e) {
            throw ExceptionFactory.create(e, HIBERNATE_SEARCH_ERROR, "Failed to find user with Email");
        }

        return u;  // there should only be one user
    }

    public void addUser(User user) {
        Session session = sessionFactory.getCurrentSession();
        try {
            session.save(user);  // depending on the id
        } catch (Exception e) {
            Log.getLogger().fatal("failed to create new user");
            Log.getLogger().fatal(e.getMessage());
        }
    }

    public Boolean deleteUserByEmail(String email) {
        Session session = sessionFactory.getCurrentSession();
        Boolean succeeded = true;

        try{
            Query query = session.createQuery("DELETE FROM User WHERE user_email=:userEmail");  // where _tableField=:_parameter
            query.setParameter("userEmail", email);
            query.executeUpdate();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            succeeded = false;
        }
        return succeeded;
    }


    public void updateUserState(User user) {
        Session session = sessionFactory.getCurrentSession();
        try {
            session.update(user);  // depending on the id
        } catch (Exception e) {
            throw ExceptionFactory.create(e, HIBERNATE_UPDATE_ERROR);
        }
    }

    /**
     * open a new session to retrieve user with availabilities
     * @param u
     * @return
     */
    public User initAvailabilities(User u) {
        Session session = sessionFactory.getCurrentSession();
        User userToInit = session.get(User.class, u.getId());
        Hibernate.initialize(userToInit.getAvailability());
        return userToInit;
    }
}
