package data.access.daos;

import data.access.tables.store.Store;
import exceptions.ExceptionFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import static exceptions.ExceptionType.HIBERNATE_SEARCH_ERROR;
import static exceptions.ExceptionType.HIBERNATE_UPDATE_ERROR;

@Repository
public class StoreDAO {
    @Autowired
    SessionFactory sessionFactory;

    public Store getStoreById(int storeId) {
        Session session = sessionFactory.getCurrentSession();
        Store s = session.get(Store.class, storeId);
        return s;
    }

    public Store getStoreByStoreName(String storeName) {
        Session session = sessionFactory.getCurrentSession();
        Query q = session.createQuery("FROM Store WHERE store_name=:storeName").setParameter("storeName", storeName);

        Store s;
        try {
            s = (Store) q.getSingleResult();
        } catch (Exception e) {
            throw ExceptionFactory.create(e, HIBERNATE_SEARCH_ERROR);
        }
        return s;
    }

    public int getStoreIdByStoreName(String storeName) {
        return getStoreByStoreName(storeName).getStoreId();
    }

    public boolean saveStore(Store store) {
        Boolean succeeded = true;
        Session session = sessionFactory.getCurrentSession();
        try {
            session.save(store);  // depending on the id
        } catch (Exception e) {
            succeeded = false;
            throw ExceptionFactory.create(e, HIBERNATE_UPDATE_ERROR);
        }
        return succeeded;
    }

}
