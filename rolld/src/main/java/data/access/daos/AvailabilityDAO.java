package data.access.daos;

import data.access.tables.user.Availability;
import data.access.tables.user.User;
import helper.Log;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class AvailabilityDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public Boolean addAvailability(Availability av) {
        Boolean succeeded = true;
        Session session = sessionFactory.getCurrentSession();
        try {
            session.save(av);  // depending on the id
        } catch (Exception e) {
            Log.getLogger().fatal("failed to create new availability");
            Log.getLogger().fatal(e.getMessage());
            succeeded = false;
        }
        return succeeded;
    }

    public Boolean deleteAvailabilityByUserId(String user_id) {
        Session session = sessionFactory.getCurrentSession();
        Boolean succeeded = true;

        try{
            Query query = session.createQuery("DELETE FROM AVAILABILITY WHERE user_id=:userId");  // where _tableField=:_parameter
            query.setParameter("userId", user_id);
            query.executeUpdate();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            succeeded = false;
        }
        return succeeded;
    }


}
