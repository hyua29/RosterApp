package data.access.daos;

import data.access.tables.token.InvitationToken;
import exceptions.ExceptionFactory;
import exceptions.ExceptionType;
import helper.Log;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public class InvitationTokenDAO {

    @Autowired
    SessionFactory sessionFactory;

    public void saveInvitationToken(InvitationToken newToken) {
        Session session = sessionFactory.getCurrentSession();
        try {
            session.save(newToken);
        } catch (Exception e) {
            throw ExceptionFactory.create(e, ExceptionType.HIBERNATE_UPDATE_ERROR, "Failed to save new invitation token");
        }
    }

    /**
     * get invitation token by token string
     * @param tokenToSearch
     * @return null if doesn't exist or expired
     */
    public InvitationToken getTokenByToken(String tokenToSearch) {
        Session session = sessionFactory.getCurrentSession();
        InvitationToken t = session.get(InvitationToken.class, tokenToSearch);
        if(t == null || t.isExpired()) {
            Log.getLogger().error("token doesn't exist or expired");
            throw ExceptionFactory.create(ExceptionType.HIBERNATE_SEARCH_ERROR);
        }

        return session.get(InvitationToken.class, tokenToSearch);
    }

    /**
     * Immediately expire the token
     */
    public void expireToken(InvitationToken token) {
        Session session = sessionFactory.getCurrentSession();
        token.setExpiryDate(new Date(0));
        try {
            session.update(token);
        } catch (Exception e) {
            throw ExceptionFactory.create(e, ExceptionType.HIBERNATE_UPDATE_ERROR, "Failed to expiry the token");
        }
    }


}
