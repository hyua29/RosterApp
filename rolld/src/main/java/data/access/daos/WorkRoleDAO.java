package data.access.daos;

import data.access.tables.store.WorkRole;
import exceptions.ExceptionFactory;
import exceptions.ExceptionType;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class WorkRoleDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public void addRole(WorkRole workRole) {
        Session session = sessionFactory.getCurrentSession();
        try {
            session.save(workRole);
        } catch (Exception e) {
            throw ExceptionFactory.create(e, ExceptionType.HIBERNATE_UPDATE_ERROR);
        }
    }

    public List<WorkRole> getRolesByStoreId(int storeId) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM WorkRole WHERE store_id=:storeId").setParameter("storeId", storeId);
        return (List<WorkRole>) query.getResultList();
    }

}
