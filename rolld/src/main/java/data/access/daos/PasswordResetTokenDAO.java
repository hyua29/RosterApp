package data.access.daos;


import data.access.tables.token.PasswordResetToken;
import data.access.tables.user.User;
import exceptions.ExceptionFactory;
import exceptions.ExceptionType;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.UUID;

@Repository
public class PasswordResetTokenDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public PasswordResetToken createPasswordResetTokenForUser(User user) {

        PasswordResetToken resetToken = new PasswordResetToken(user);

        Session session = sessionFactory.getCurrentSession();

        try {
            session.saveOrUpdate(resetToken);  // depending on the id, one user can only have one valid reset token
        } catch (Exception e) {
            throw ExceptionFactory.create(e, ExceptionType.HIBERNATE_UPDATE_ERROR, "Failed to update reset token");
        }

        return resetToken;
    }

    public PasswordResetToken getTokenByUserId(int id) {
        Session session = sessionFactory.getCurrentSession();
        PasswordResetToken u = session.get(PasswordResetToken.class, id);

        if(u == null)
            throw ExceptionFactory.create(ExceptionType.HIBERNATE_SEARCH_ERROR, "Failed to find token by user id");
        return u;
    }

    public PasswordResetToken getTokenByToken(String tokenToSearch){
        Session session = sessionFactory.getCurrentSession();
        Query q = session.createQuery(" FROM PasswordResetToken WHERE token=:token").setParameter("token", tokenToSearch);


        PasswordResetToken t;
        try {
            t = (PasswordResetToken) q.getSingleResult();
        } catch (Exception e) {
            throw ExceptionFactory.create(e, ExceptionType.HIBERNATE_SEARCH_ERROR, "reset token does not exist");
        }

        return t;
    }

    /**
     * reset the expiry date
     * @param passwordResetToken
     */
    public boolean resetExpiry(PasswordResetToken passwordResetToken) {
        boolean succeeded = true;
        passwordResetToken.resetExpiry();
        Session session = sessionFactory.getCurrentSession();
        try {
            session.update(passwordResetToken);
        } catch (Exception e) {
            throw ExceptionFactory.create(e, ExceptionType.HIBERNATE_SEARCH_ERROR, "Failed to reset expiry date");
        }
        return succeeded;
    }

    /**
     * reset the token attribute of PasswordResetToken
     */
    public boolean resetToken(PasswordResetToken passwordResetToken) {
        boolean succeeded = true;
        passwordResetToken.setToken(UUID.randomUUID().toString());
        Session session = sessionFactory.getCurrentSession();
        try {
            session.update(passwordResetToken);
        } catch (Exception e) {
            throw ExceptionFactory.create(e, ExceptionType.HIBERNATE_SEARCH_ERROR, "Failed to reset the reset token");
        }
        return succeeded;
    }


    /**
     * immediately expire the reset token
     * @param t
     */
    public void expireToken(PasswordResetToken t) {
        Session session = sessionFactory.getCurrentSession();

        t.setExpiryDate(new Date(0));
        try {
            session.update(t);
        } catch (Exception e) {
            throw ExceptionFactory.create(e, ExceptionType.HIBERNATE_UPDATE_ERROR, "Failed to expire reset token");
        }
    }
}
