package data.access.services;

import data.access.daos.UserDAO;
import data.access.tables.user.User;
import data.access.tables.userstoreauthority.UserStoreAuthority;
import data.access.tables.userstoreauthority.UserStoreAuthorityPK;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserManagementService {

    @Autowired
    UserDAO userDao;

    @Transactional  // Spring would take care of starting transactions, committing transactions or rolling back transactions when exceptions are thrown
    public List<User> getAllUsers() {
        List<User> userList = userDao.getAllUsers();
        return userList;
    }

    @Transactional
    public Boolean deleteUserByEmail(String email) {
        return userDao.deleteUserByEmail(email);
    }

    @Transactional
    public void addUser(User user) {
        userDao.addUser(user);
    }

    @Transactional
    public User getUserByEmail(String email) {
        return userDao.getUserByEmail(email);
    }

    /**
     * take user input to link a user to new authority and store
     * @param userEmail
     * @param storeId
     * @param authorityId
     * @return
     */
    @Transactional
    public boolean addUserToStoreWithAuthority(String userEmail, int storeId, int authorityId) {
        User user = userDao.getUserByEmail(userEmail);
        if(user == null)
            return false;

        List<UserStoreAuthority> usaList = new ArrayList<UserStoreAuthority>(3);
        usaList.add(new UserStoreAuthority(new UserStoreAuthorityPK(user.getId(), storeId, authorityId)));
        user.setUserStoreAuthorities(usaList);
        userDao.updateUserState(user);

        return true;

    }
    /**
     * take in data and create one only row
     * generate a list of rows to store to user_store_authority table
     */
    private List<UserStoreAuthority> createUSAKeyForNewUser(int userId, int storeId, int authorityId) {
        List<UserStoreAuthority> usaList = new ArrayList<UserStoreAuthority>(3);
        usaList.add(new UserStoreAuthority(new UserStoreAuthorityPK(userId, storeId, authorityId)));

        return usaList;
    }

    /**
     * Get user with availabilities
     * @param u
     * @return
     */
    @Transactional
    public User initAvailabilities(User u) {
        return userDao.initAvailabilities(u);
    }


}
