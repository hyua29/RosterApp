package data.access.services;

import data.access.tables.user.User;
import exceptions.ExceptionFactory;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static exceptions.ExceptionType.USER_MAIL_NOT_VALID;

//TODO: The scope of this is too vague, consider breaking it down
@Service
public class MailService {

    public final static String EMAIL_FROM = "rolldgcr@gmail.com";

    @Autowired
    private JavaMailSenderImpl mailSender;

    @Autowired
    private SimpleMailMessage preConfiguredMessage;

    @Autowired
    UserManagementService userManagementService;

    public void sendMail(String from, String to, String subject, String msg) {
        SimpleMailMessage message = new SimpleMailMessage();
    }

    /**
     * notify all users the roster is out
     * TODO: handle the error thrown due to invalid email format
     */
    public void notifyRosterRelease() {
        SimpleMailMessage mailMessage = new SimpleMailMessage(preConfiguredMessage);
        mailMessage.setSubject("This Week's Roster DoNotReply");
        mailMessage.setText("This is the re-direct link");

        List<String> emailAddress = new ArrayList<String>(30);

        List<User> users = userManagementService.getAllUsers();
        for(User u : users) {
            if(u.getEmail() != null) {
                emailAddress.add(u.getEmail());
            }
            else
                throw ExceptionFactory.create(new ValueException("this is not a valid user email"), USER_MAIL_NOT_VALID);
        }

        mailMessage.setTo(emailAddress.toArray(new String[emailAddress.size()]));
        mailSender.send(mailMessage);
//        mailMessage.setTo(String.valueOf(emailAddress));

    }


    /**
     * notify all users the roster is out
     * TODO: handle the error thrown due to invalid email format
     */
    public boolean notifyRosterRelease2() {
        boolean succeeded = true;

        SimpleMailMessage mailMessage = new SimpleMailMessage(preConfiguredMessage);
        mailMessage.setSubject("This Week's Roster DoNotReply");
        mailMessage.setText("This is the re-direct link");

        List<String> emailAddress = new ArrayList<String>(30);

        List<User> users = userManagementService.getAllUsers();
        for(User u : users) {
            if(u.getEmail() != null) {
                emailAddress.add(u.getEmail());
                mailMessage.setTo(u.getEmail());
                mailSender.send(mailMessage);
            }
            else {
                succeeded = false;
            }
        }

//        mailMessage.setTo(String.valueOf(emailAddress));
        return false;
    }



}
