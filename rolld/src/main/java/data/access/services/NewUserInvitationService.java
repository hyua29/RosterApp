package data.access.services;

import data.access.daos.InvitationTokenDAO;
import data.access.daos.StoreDAO;
import data.access.daos.UserAuthorityDAO;
import data.access.daos.UserDAO;
import data.access.tables.token.InvitationToken;
import data.access.tables.user.User;
import data.access.tables.userstoreauthority.UserStoreAuthority;
import data.access.tables.userstoreauthority.UserStoreAuthorityPK;
import exceptions.ApplicationSpecificException;
import exceptions.ExceptionFactory;
import exceptions.ExceptionType;
import helper.HTML;
import helper.Log;
import data.access.tables.user.UserDetailsByReceiver;
import data.access.tables.user.UserDetailsBySender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class NewUserInvitationService {

    @Value("${app.domain}")
    private String baseURL;

    @Autowired
    private JavaMailSenderImpl mailSender;

    @Autowired
    private StoreDAO storeDAO;

    @Autowired
    private UserAuthorityDAO userAuthorityDAO;

    @Autowired
    private InvitationTokenDAO invitationTokenDAO;

    @Autowired
    private UserDAO userDAO;
    /**
     * Sends a unique sign up url to the user
     * @param userEmail the user to send to
     * @param token the unique token forming the unique sign up url
     * @return state of this action
     */
    public void sendInvitationToUser(String userEmail, String token) {
        final String INVITATION_URL = baseURL + "/authentication/verify-invitation?token=" + token;

        MimeMessage message = mailSender.createMimeMessage();
        try {
            message.setContent("Hi, Click " + HTML.constructHyperLink("HERE", INVITATION_URL) + " to sign up", "text/HTML");
            MimeMessageHelper helper = new MimeMessageHelper(message, false, "utf-8");
            helper.setSubject("Roll'd Sign Up");
            helper.setTo(userEmail);
            helper.setFrom(MailService.EMAIL_FROM);
        } catch (MessagingException e) {
            throw ExceptionFactory.create(e, ExceptionType.EMAIL_CONSTRUCT_ERROR);
        }

        try {
            mailSender.send(message);
            Log.getLogger().info("Invitation email sent");
        } catch (Exception e) {
            throw ExceptionFactory.create(e, ExceptionType.EMAIL_SEND_ERROR);
        }
    }

    /**
     * create a token based on user input
     * return null if save action is failed or data isn't valid
     */
    @Transactional
    public InvitationToken createTokenForNewUser(UserDetailsBySender userDetails) {
        int storeId = storeDAO.getStoreIdByStoreName(userDetails.getStoreName());
        int authorityId = userAuthorityDAO.getUserAuthorityIdByName(userDetails.getAuthority());
        InvitationToken invitationToken = new InvitationToken(userDetails.getEmail(),
                storeId, authorityId, userDetails.getEmployType());

        invitationTokenDAO.saveInvitationToken(invitationToken);

        return invitationToken;
    }

    /**
     * verify whether token exists and not expired
     * @param token
     * @return
     */
    @Transactional
    public boolean verifyResetToken(String token) {
        if(token.equals("111"))  // FIXME: this is a back door allowing the verification to be bypassed
            return true;
        InvitationToken i;
        try {
            i = invitationTokenDAO.getTokenByToken(token);
        } catch (ApplicationSpecificException e) {
            return false;
        }

        return !i.isExpired();
    }

    @Transactional
    public InvitationToken getTokenByToken(String token) {
        return invitationTokenDAO.getTokenByToken(token);
    }

    /**
     * create a new user with info from the email receiver and the token
     * expires the token once new user has been created
     */
    @Transactional
    public boolean createNewUser(UserDetailsByReceiver userDetailsByReceiver, String iToken) {

        InvitationToken invitationToken = invitationTokenDAO.getTokenByToken(iToken);

        // create  new user based on the user input and the invitation token
        User user = new User(userDetailsByReceiver.getfName(), userDetailsByReceiver.getlName(), new Date(),
                invitationToken.getInviteEmail(), userDetailsByReceiver.getPassword(), invitationToken.getEmpType(), "active");

//        List<UserStoreAuthority> usaList = createUSAKeyForNewUser(user.getId(), invitationToken.getStoreId(), invitationToken.getAuthorityId());
//        user.setUserStoreAuthorities(usaList);  // set the store and authority

        userDAO.addUser(user);
        invitationTokenDAO.expireToken(invitationToken);
        return true;
    }

    /**
     * TODO: replace the one above with this one after core features have been finished
     * take in data and create one or more than one rows
     * generate a list of rows to store to user_store_authority table
     */
    private List<UserStoreAuthority> createUSAKeyForNewUser(int userId, List<Integer> storeIds, List<Integer> authorityIds) {
        List<UserStoreAuthority> usaList = new ArrayList<UserStoreAuthority>(3);
        for(int i : storeIds) {
            for(int j : authorityIds)
                usaList.add(new UserStoreAuthority(new UserStoreAuthorityPK(userId, i, j)));
        }

        return usaList;
    }

    /** //TODO: consider using the one in UserManagementService
     * take in data and create one only row
     * generate a list of rows to store to user_store_authority table
     */
    private List<UserStoreAuthority> createUSAKeyForNewUser(int userId, int storeId, int authorityId) {
        List<UserStoreAuthority> usaList = new ArrayList<UserStoreAuthority>(3);
        usaList.add(new UserStoreAuthority(new UserStoreAuthorityPK(userId, storeId, authorityId)));

        return usaList;
    }
}