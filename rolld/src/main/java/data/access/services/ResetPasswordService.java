package data.access.services;

import data.access.daos.PasswordResetTokenDAO;
import data.access.daos.UserDAO;
import data.access.tables.token.PasswordResetToken;
import data.access.tables.user.User;
import exceptions.ApplicationSpecificException;
import exceptions.ExceptionFactory;
import exceptions.ExceptionType;
import helper.HTML;
import helper.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class ResetPasswordService {

    @Value("${app.domain}")
    private String baseURL;

    @Autowired
    private PasswordResetTokenDAO passwordResetTokenDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private JavaMailSenderImpl mailSender;


    /**
     * create password token for the user
     * reset the expiry time and return the old token if the token has not expired
     * reset the expiry time and token if the token has expired
     * @param userEmail search user by email
     * @return a reference of the token object stored to db
     */
    @Transactional
    public PasswordResetToken createPasswordResetTokenForUser(String userEmail) {

        User user = userDAO.getUserByEmail(userEmail);

        PasswordResetToken passwordResetToken;
        try {
            passwordResetToken = passwordResetTokenDAO.getTokenByUserId(user.getId());
        } catch (ApplicationSpecificException e) {
            if(e.getExceptionType() == ExceptionType.HIBERNATE_SEARCH_ERROR)  // no prior token
                passwordResetToken = passwordResetTokenDAO.createPasswordResetTokenForUser(user);
            else
                throw ExceptionFactory.create(e, e.getExceptionType());
        }

        if (passwordResetToken.isExpired()) // reset the toke if the token has expired
            passwordResetTokenDAO.resetToken(passwordResetToken);

        passwordResetTokenDAO.resetExpiry(passwordResetToken);  // reset the expiry date

        return passwordResetToken;
    }

    /**
     * send password reset token to the user
     * @param userEmail
     * @param resetToken
     */
    public void sendTokenToUser(String userEmail, String resetToken) {

        final String RESET_PASSWORD_URL = baseURL + "/authentication/verify-reset?reset-token=" + resetToken;

        MimeMessage message = mailSender.createMimeMessage();
        try {
            message.setContent("Hi, Click " + HTML.constructHyperLink("HERE", RESET_PASSWORD_URL) + " to change your password", "text/HTML");
            MimeMessageHelper helper = new MimeMessageHelper(message, false, "utf-8");
            helper.setSubject("Roll'd Reset Password");
            helper.setTo(userEmail);
            helper.setFrom(MailService.EMAIL_FROM);
        } catch (MessagingException e) {
            throw ExceptionFactory.create(e, ExceptionType.EMAIL_CONSTRUCT_ERROR);
        }

        try {
            mailSender.send(message);
            Log.getLogger().info("reset email sent");
        } catch (Exception e) {
            throw ExceptionFactory.create(e, ExceptionType.EMAIL_SEND_ERROR);
        }
    }


    /**
     * verify whether token exists and not expired
     * @param token
     * @return
     */
    @Transactional
    public boolean verifyResetToken(String token) {
        if(token.equals("111"))  // FIXME: this is a back door allowing the verification to be bypassed
            return true;

        PasswordResetToken t;
        try {
            t = passwordResetTokenDAO.getTokenByToken(token);
        } catch (ApplicationSpecificException e) {  // token doesn't exist
            return false;
        }

        return !t.isExpired();
    }

    /**
     * reset the password if two passwords are matching
     * expire the token after the password has been successfully reset
     * @param newPassword
     * @param passwordConfirm
     * @param resetToken
     * @return
     */
    @Transactional
    public boolean resetPassword(String newPassword, String passwordConfirm, String resetToken) {
        if (!newPassword.equals(passwordConfirm))
            return false;

        PasswordResetToken t = passwordResetTokenDAO.getTokenByToken(resetToken);
        User u = t.getUser();

        u.setPassword(newPassword);

        userDAO.updateUserState(u);
        passwordResetTokenDAO.expireToken(t);

        return true;
    }
}
