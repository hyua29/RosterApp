package data.access.services;

import data.access.daos.StoreDAO;
import data.access.daos.UserDAO;
import data.access.daos.UserStoreAuthorityDao;
import data.access.daos.WorkRoleDAO;
import data.access.tables.store.AjaxRequestWorkRole;
import data.access.tables.store.Store;
import data.access.tables.store.WorkRole;
import data.access.tables.user.User;
import helper.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class StoreManagementService {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private StoreDAO storeDAO;

    @Autowired
    private UserStoreAuthorityDao userStoreAuthorityDao;

    @Autowired
    private UserManagementService userManagementService;

    @Autowired
    private WorkRoleDAO workRoleDAO;

    /**
     * check whether the current user is the manager of the designated store
     * @param user
     * @param storeName
     * @return
     */
    @Transactional
    public boolean isManagerOfTheStore(User user, String storeName) {
        return userStoreAuthorityDao.isManager(user.getId(), storeDAO.getStoreByStoreName(storeName).getStoreId());
    }

    /**
     * check whether the current user is the owner of the designated store
     * @param user
     * @param storeName
     * @return
     */
    @Transactional
    public boolean isOwnerOfTheStore(User user, String storeName) {
        return userStoreAuthorityDao.isOwner(user.getId(), storeDAO.getStoreByStoreName(storeName).getStoreId());
    }

    /**
     * check whether the current user is the manager or owner of the designated store
     * @param user
     * @param storeName
     * @return
     */
    @Transactional
    public boolean isManagerOrOwner(User user, String storeName) {
        if(isManagerOfTheStore(user, storeName))
            return true;
        else if(isOwnerOfTheStore(user, storeName))
            return true;
        else
            return false;
    }

    /**
     *
     * @param storeName
     * @return All the users in the store with email as the key
     */
    @Transactional
    public Map<String, User> getEmployeeDetailsFromCurrentStore(String storeName) {
        Map<String, User> users = new HashMap<String, User>();
        List<User> userList = userStoreAuthorityDao.getEmployeesFromStore(storeDAO.getStoreIdByStoreName(storeName), true);
        for (User u : userList)
            users.put(u.getEmail(), u);
        return users;
    }

    /**
     *
     * @param userEmail the manager/owner's email
     * @return stores the current user is in charge of
     */
    @Transactional
    public List<String> getStoreNames(String userEmail) {
        return userStoreAuthorityDao.getStoreNames(userDAO.getUserByEmail(userEmail).getId());
    }

    @Transactional
    public List<WorkRole> getWorkRoleByStoreName(String storeName) {
        return workRoleDAO.getRolesByStoreId(storeDAO.getStoreIdByStoreName(storeName));
    }

    /**
     * create a work role for the given store
     * @param requestWorkRole
     * @return
     */
    @Transactional
    public WorkRole createWorkRole(AjaxRequestWorkRole requestWorkRole) {
        WorkRole workRole = new WorkRole(requestWorkRole);
        workRole.setStore(storeDAO.getStoreByStoreName(requestWorkRole.getStoreName()));
        workRoleDAO.addRole(workRole);
        return workRole;
    }
}
