package data.access.services.loginmanagement;

import data.access.services.UserManagementService;
import data.access.tables.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class CustomLogin implements UserDetailsService {

    @Autowired
    private UserManagementService userManagementService;

    @Transactional(readOnly=true)
    public UserDetails loadUserByUsername(String emailAsUsername) throws UsernameNotFoundException {
        User user = userManagementService.getUserByEmail(emailAsUsername);

        if(user==null){
            System.out.println("User not found");
            throw new UsernameNotFoundException("Username not found");
        }

        System.out.println("-----------------------------------");
        System.out.println(user.getAuthorityList());
        //User(java.lang.String username, java.lang.String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, java.util.Collection<? extends GrantedAuthority> authorities)
        //TODO: check user state to validate
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
                true, true, true, true, user.getAuthorityList());

    }

//    //   can adapt authorities from database if incompatible formats are used
//    private List<GrantedAuthority> getGrantedAuthorities(User user){
//        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
//
//        for(UserAuthority role: user.getUserAuthorityList()){
//            System.out.println("UserProfile : "+ role.getAuthority());
//            authorities.add(new SimpleGrantedAuthority("ROLE_"+role.getAuthority()));
//        }
//        return authorities;
//    }

}

