package data.access.tables;

import org.joda.time.LocalDate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TableUtil {

    /**
     * converts a string "HH:mm:ss" to a Date type "yyyy-MM-dd HH:mm:ss"
     * << the date is the current date when the data was saved
     * @param time format is "HH:mm:ss"
     * @return date
     */
    public static Date convertTimeStringToDateType(String time){
        LocalDate date= new LocalDate();
        time = date.toString() + " " + time;
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date timeT = null;

        try {
            timeT = dateformat.parse(time);
        } catch (ParseException e) {
            System.out.println("Cannot parse availability time.");
        }
        return timeT;
    }

    /**
     * extract only time from Date type
     * @param date
     * @return format "HH:mm:ss"
     */
    public static String extractTimeStringFromDate(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(date);
    }


    /**
     * Validate that the day input inserted by programmer is correct
     * @param day can be either 'MON', 'TUE', 'WED', 'THU',...
     * @return true if the input is valid, false if otherwise
     */
    private static Boolean validateDayInput(String day){
        String[] dayArray = {"MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"};
        boolean dayValueValidation = false;
        for (int i = 0; i< dayArray.length; i++){
            if (day == dayArray[i]){
                dayValueValidation = true;
                return true;
            }
        }
        System.out.println("setAvailableDay() not successful.");
        return false;
    }

    /**
     * parses a date (DOB) from string into a date type
     * @param string format is eg: "16-04-1996"
     * @return a date object
     */
    public static Date parseDobIntoDate(String string){
        SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");
        String date_input = string;
        Date dob = null;

        try {
            dob = dateformat.parse(date_input);
        } catch (ParseException e) {
            System.out.println("Cannot parse date. insertUser1 test");
        }

        return dob;
    }

}
