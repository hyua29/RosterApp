package data.access.tables.user;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * this class holds the fields filled in by the person who receives the invitation token
 */
public class UserDetailsByReceiver {

    private String fName;

    private String lName;

    private String password;

    private String passwordConfirm;

    private Date doB;

    public UserDetailsByReceiver(String fName, String lName, String password, String passwordConfirm, Date doB) {
        this.fName = fName;
        this.lName = lName;
        this.password = password;
        this.passwordConfirm = passwordConfirm;
        this.doB = doB;
    }

    public UserDetailsByReceiver() {}

    public String getfName() {
        return fName;
    }

    public String getlName() {
        return lName;
    }

    public String getPassword() {
        return password;
    }

    public Date getDoB() {
        return doB;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setDoB(String doB) {
        try {
            this.doB = new SimpleDateFormat("dd-MM-yyyy").parse(doB);
        } catch (ParseException e) {
            e.printStackTrace();  // TODO: handle the exception
        }
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    @Override
    public String toString() {
        return "UserDetailsByReceiver{" +
                "fName='" + fName + '\'' +
                ", lName='" + lName + '\'' +
                ", password='" + password + '\'' +
                ", doB=" + doB +
                '}';
    }
}
