package data.access.tables.user;

import com.fasterxml.jackson.annotation.JsonIgnore;

import com.fasterxml.jackson.annotation.JsonView;
import data.access.tables.authority.UserAuthority;
import data.access.tables.userstoreauthority.UserStoreAuthority;
import helper.View;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is a template of how a class should be annotated so that it can be stored to and
 * retrieved from database by hibernate
 */
// must be java persistence
@Entity  //This is an entity that that can be mapped to database
@Table(name="USER")  // Table this entity goes to
public class User {

    @JsonIgnore
    @Id // is primary key
    @Column(name="user_id")
    private int id;

    @JsonView(View.Public.class)
    @Column(name="user_fname", nullable = false)
    private String fName;

    @JsonView(View.Public.class)
    @Column(name="user_lname")
    private String lName;

    @JsonView(View.Public.class)
    @Column(name="user_dob", nullable = false)
    private Date dOB;

    @JsonView(View.Public.class)
    @Column(name="user_email", nullable = false, unique = true)
    private String email;


    @JsonIgnore
    @Column(name="user_password", nullable = false)
    private String password;

    @JsonView(View.Public.class)
    @Column(name="user_employment_type", nullable = false)
    private String employType;

    @JsonView(View.Public.class)
    @Column(name="user_state", nullable = false)
    private String state;

    //@JsonIgnoreProperties({"user", "userAuthority", "store"})
    @JsonIgnore
    @OneToMany(mappedBy = "user", fetch= FetchType.EAGER, cascade = {CascadeType.ALL})  // define which attribute(in the other class) referencing this class
    private List<UserStoreAuthority> userStoreAuthorities;

    @Transient
    private List<UserAuthority> authorityList;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL) // FIXME: check whether the cascade type is correct
    private List<Availability> availability;

    public User(){
        // Must have this no-arg class so that hibernate can retrieveObjectByID data from database and construct java object
        // with setters and getters
    }

    public User(String fName, String lName, Date dOB, String email, String password, String employType, String state) {
        // List<UserStoreAuthority> userStoreAuthorities,
        //user id shall be left as null as it is auto-incremented in db, hence there is no 'this.id = ...'
        this.fName = fName;
        this.lName = lName;
        this.dOB = dOB;
        this.email = email;
        this.password = password;
        this.employType = employType;
        this.state = state;
        //this.userStoreAuthorities = userStoreAuthorities;
    }

    public User(String name, String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public Date getdOB() {
        return dOB;
    }

    public void setdOB(Date dOB) {
        this.dOB = dOB;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmployType() {
        return employType;
    }

    public void setEmployType(String employType) {
        this.employType = employType;
    }

    public void setEmployType(EmploymentType employType) {
        this.employType = employType.name();
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<UserStoreAuthority> getUserStoreAuthorities() {
        return userStoreAuthorities;
    }

    public void setUserStoreAuthorities(List<UserStoreAuthority> userStoreAuthorities) {
        this.userStoreAuthorities = userStoreAuthorities;
     //   this.setUserAuthorityList();
    }

    /**
     * query the UserStoreAuthority and extract the UserAuthority
     * @return authorities of this user
     * TODO: test required
     */
    public List<UserAuthority> getAuthorityList() {
        if(authorityList == null) {
            this.authorityList = new ArrayList<UserAuthority>(3);
            for (UserStoreAuthority m : this.getUserStoreAuthorities()) {
                if (!this.authorityList.contains(m.getUserAuthority()))  // contains() uses equals() to evaluate objects
                    this.authorityList.add(m.getUserAuthority());
            }
        }
        return this.authorityList;
    }

    public void setAuthorityList(List<UserAuthority> userAuthorities) {
        this.authorityList = userAuthorities;
    }

//    public void setUserAuthorityList() {
//        this.userAuthorityList = new ArrayList<UserAuthority>(3);
//        for(UserStoreAuthority m : this.getUserStoreAuthorities()) {
//            if (!this.userAuthorityList.contains(m.getUserAuthority()))  // contains() uses equals() to evaluate objects
//                this.userAuthorityList.add(m.getUserAuthority());
//        }
//    }

    public List<Availability> getAvailability() {
        return availability;
    }

    public void setAvailability(List<Availability> availability) {
        this.availability = availability;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", fName='" + fName + '\'' +
                ", lName='" + lName + '\'' +
                ", dOB=" + dOB +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", employType='" + employType + '\'' +
                ", state='" + state + '\'' +
                ", userStoreAuthorities=" + userStoreAuthorities +
                ", authorityList=" + authorityList +
        //        ", availability=" + availability +
                '}';
    }
}
