package data.access.tables.user;

import java.util.Date;

import static data.access.tables.authority.AuthorityType.EMPLOYEE;

/**
 * this class holds the fields filled in by the person who sends the invitation token
 */
public class UserDetailsBySender {

    private String email;

    private String employType;

    private String state;

    private String storeName;

    private String authority;

    // TODO: remove hard-coded value and handle them in another way
    public UserDetailsBySender(String email, String employType, String state,
                               String storeName, String authority) {
        this.email = email;
        this.employType = employType;
        this.state = "active";
        this.storeName = storeName;
        this.authority = EMPLOYEE.name();
    }

    // TODO: remove hard-coded value and handle them in another way
    public UserDetailsBySender(String email, String employType,
                               String storeName) {
        this.email = email;
        this.employType = employType;
        this.state = "active";
        this.storeName = storeName;
        this.authority = EMPLOYEE.name();
    }

    public UserDetailsBySender() {
        this.state = "active";
        this.authority = EMPLOYEE.name();
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setEmployType(String employType) {
        this.employType = employType;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public String getEmail() {
        return email;
    }

    public String getEmployType() {
        return employType;
    }

    public String getState() {
        return state;
    }

    public String getStoreName() {
        return storeName;
    }

    public String getAuthority() {
        return authority;
    }

    @Override
    public String toString() {
        return "UserDetailsBySender{" +
                "email='" + email + '\'' +
                ", employType='" + employType + '\'' +
                ", state='" + state + '\'' +
                ", storeName='" + storeName + '\'' +
                ", authority='" + authority + '\'' +
                '}';
    }
}
