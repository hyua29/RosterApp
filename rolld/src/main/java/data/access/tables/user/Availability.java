package data.access.tables.user;

import com.fasterxml.jackson.annotation.JsonIgnore;

import data.access.tables.TableUtil;
import helper.View;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="AVAILABILITY")
public class Availability {

    @Id
    @Column(name = "av_id")
    private int availabilityId;

    @Column(name = "av_day", nullable = false)
    private String availableDay;

    @Column(name = "av_time_start", nullable = false)
    private Date availableTimeStart;

    @Column(name = "av_time_end", nullable = false)

    private Date availableTimeEnd;

    @JsonIgnore
    @ManyToOne(fetch= FetchType.EAGER, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)
    private User user;


    /**
     * contructor method
     *
     * @param av_day
     * @param av_time_start example format is "00:11:22"
     * @param av_time_end example format is "00:11:22"
     */
    public Availability(String av_day, String av_time_start, String av_time_end) {
        // List<UserStoreAuthority> userStoreAuthorities,
        //availability_id shall be left as null as it is auto-incremented in db, hence there is no 'this.id = ...'
        this.availableDay = av_day;
        this.availableTimeStart = TableUtil.convertTimeStringToDateType(av_time_start);
        this.availableTimeEnd = TableUtil.convertTimeStringToDateType(av_time_end);
    }

    public Availability() {
    }

    public String getAvailableDay() {
        return availableDay;
    }

    public void setAvailableDay(String availableDay) {
        this.availableDay = availableDay;
    }

    public Date getAvailableTimeStart() {
        //SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        //return sdf.format(availableTimeStart);
        return availableTimeStart;
    }

    public void setAvailableTimeStart(Date availableTimeStart) {
        this.availableTimeStart = availableTimeStart;
    }

    public Date getAvailableTimeEnd() {
        return availableTimeEnd;
    }

    public void setAvailableTimeEnd(Date availableTimeEnd) {
        this.availableTimeEnd = availableTimeEnd;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getAvailabilityId() {
        return availabilityId;
    }

    public void setAvailabilityId(int availabilityId) {
        this.availabilityId = availabilityId;
    }

    @Override
    public String toString() {
        return "Availability{" +
                "availabilityId=" + availabilityId +
                ", availableDay='" + availableDay + '\'' +
                ", availableTimeStart=" + availableTimeStart +
                ", availableTimeEnd=" + availableTimeEnd +
                ", user=" + user +
                '}';
    }
}
