package data.access.tables.authority;

import com.fasterxml.jackson.annotation.JsonView;
import helper.View;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Column;
import javax.persistence.Id;

@javax.persistence.Entity  //This is an entity that that can be mapped to database
@javax.persistence.Table(name="USER_AUTHORITY")  // Table this entity goes to
public class UserAuthority implements GrantedAuthority {

    private static final String prefix = "ROLE_";  // prefix of authority

    @Id // is primary key
    @Column(name="userauthority_id")
    private int userAuthorityId;

    @JsonView(View.Public.class)
    @Column(name="userauthority_name", nullable = false)
    private String authority;

    public UserAuthority(){}

    public String getAuthority() {
        return prefix + authority;
    }

    /**
     * store as uppercase
     * @param authority
     */
    public void setAuthority(String authority) {
        this.authority = authority.toUpperCase();
    }

    public int getUserAuthorityId() {
        return userAuthorityId;
    }

    public void setUserAuthorityId(int userAuthorityId) {
        this.userAuthorityId = userAuthorityId;
    }

    @Override
    public String toString() {
        return "UserAuthority{" +
                "userAuthorityId=" + userAuthorityId +
                ", authority='" + prefix + authority + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserAuthority that = (UserAuthority) o;

        return userAuthorityId == that.userAuthorityId;
    }

    @Override
    public int hashCode() {
        return userAuthorityId;
    }
}
