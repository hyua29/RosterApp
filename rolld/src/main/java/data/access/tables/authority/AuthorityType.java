package data.access.tables.authority;

public enum AuthorityType {
    EMPLOYEE("ROLE_EMPLOYEE"),
    MANAGER("ROLE_MANAGER"),
    OWNER("ROLE_OWNER");

    private final String mask;

    private AuthorityType(String mask) {
        this.mask = mask;
    }

    public String getMask() {
        return this.mask;
    }
}

