package data.access.tables.token;

import data.access.tables.user.User;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name="PASSWORD_R_TOKEN")
public class PasswordResetToken implements UniqueToken {

    private static final int VALID_DURATION = 60; // expire in 60 minutes

    @Id
    @Column(name = "user_id")
    private int userId;

    @MapsId("userId")
    @OneToOne(fetch= FetchType.EAGER, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Column(name="token", nullable = false, unique = true)
    private String token;

    @Column(name="token_expiry_date", nullable = false)
    private Date expiryDate;

    public PasswordResetToken(User u) {
        this.userId = u.getId();
        this.user = u;
        this.token = UUID.randomUUID().toString();
        this.resetExpiry();
    }

    public PasswordResetToken() {}


    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public boolean isExpired() {
        return new Date().after(this.expiryDate);
    }

    public void resetExpiry() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date()); // sets calendar time/date
        cal.add(Calendar.MINUTE, VALID_DURATION);
        expiryDate = cal.getTime();
    }

}
