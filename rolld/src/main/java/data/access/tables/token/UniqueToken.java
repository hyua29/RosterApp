package data.access.tables.token;

interface UniqueToken {

    public boolean isExpired();

}
