package data.access.tables.token;


import data.access.tables.store.Store;
import data.access.tables.authority.UserAuthority;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name="INVITATION_TOKEN")
public class InvitationToken implements UniqueToken {

    // The token remain valid for 24 hours
    private static final int VALID_DURATION = 24;

    @Id
    @Column(name = "invitation_token")
    private String invitationToken;

    @Column(name = "expiry_date", nullable = false)
    private Date expiryDate;

    @Column(name = "invite_email", nullable = false)
    private String inviteEmail;

    @Column(name = "store_id", nullable = false)
    private int storeId;

    @Column(name = "user_authority", nullable = false)
    private int authorityId;

    @MapsId("storeId")
    @OneToOne(fetch= FetchType.EAGER)
    @JoinColumn(name = "store_id", nullable = false)
    private Store store;

    @MapsId("user_authority")
    @OneToOne(fetch= FetchType.EAGER)
    @JoinColumn(name = "user_authority", nullable = false)
    private UserAuthority userAuth;

    @Column(name = "user_employment_type", nullable = false)
    private String empType;

    private InvitationToken() {
    }

    public InvitationToken(String inviteEmail, int storeId, int AuthId, String empType) {
        this.invitationToken = UUID.randomUUID().toString();
        this.inviteEmail = inviteEmail;
        this.storeId = storeId;
        this.authorityId = AuthId;
        this.empType = empType;

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date()); // sets calendar time/date
        cal.add(Calendar.HOUR, VALID_DURATION);
        this.expiryDate = cal.getTime();
    }

    public String getInvitationToken() {
        return invitationToken;
    }

    public void setInvitationToken(String invitationToken) {
        this.invitationToken = invitationToken;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getInviteEmail() {
        return inviteEmail;
    }

    public void setInviteEmail(String inviteEmail) {
        this.inviteEmail = inviteEmail;
    }

    public Store getStore() {
        return store;
    }

    public UserAuthority getUserAuth() {
        return userAuth;
    }

    public String getEmpType() {
        return empType;
    }

    public void setEmpType(String empType) {
        this.empType = empType;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public int getAuthorityId() {
        return authorityId;
    }

    public void setAuthorityId(int authorityId) {
        this.authorityId = authorityId;
    }

    public boolean isExpired() {
        return new Date().after(this.expiryDate);
    }

    @Override
    public String toString() {
        return "InvitationToken{" +
                "invitationToken='" + invitationToken + '\'' +
                ", expiryDate=" + expiryDate +
                ", inviteEmail='" + inviteEmail + '\'' +
                ", storeId=" + storeId +
                ", authorityId=" + authorityId +
                ", store=" + store +
                ", userAuth=" + userAuth +
                ", empType='" + empType + '\'' +
                '}';
    }
}