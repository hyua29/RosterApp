package data.access.tables.store;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "WORK_ROLE")
public class WorkRole {

    @Id
    @Column(name = "role_id")
    private int roleId;

    @Column(name = "role_name", nullable = false)
    private String roleName;

    @Column(name = "role_comment")
    private String roleComment;

    @Column(name = "store_id", nullable = false)
    private int storeId;

    @JsonIgnore
    @MapsId("storeId")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "store_id", referencedColumnName = "store_id")
    private Store store;

    @Column(name = "is_in_template", nullable = false)
    private boolean inTemplate;

    public WorkRole() {
    }

    public WorkRole(AjaxRequestWorkRole requestWorkRole) {
        this.roleName = requestWorkRole.getRoleName();
        this.roleComment = requestWorkRole.getRoleComment();
        this.inTemplate = requestWorkRole.isInTemplate();
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleComment() {
        return roleComment;
    }

    public void setRoleComment(String roleComment) {
        this.roleComment = roleComment;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
        this.setStoreId(store.getStoreId());
    }

    public boolean isInTemplate() {
        return inTemplate;
    }

    public void setInTemplate(boolean inTemplate) {
        this.inTemplate = inTemplate;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }
}

