package data.access.tables.store;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "STORE")
public class Store {

    @Id
    @Column(name = "store_id")
    private int storeId;

    @Column(name = "store_name", nullable = false)
    private String storeName;

    @Column(name = "store_street_name", nullable = false)
    private String streetName;

    @Column(name = "store_suburb")
    private String storeSuburb;

    @Column(name = "store_postcode")
    private int storePostcode;

    @Column(name = "store_country")
    private String storeCountry;

    @OneToMany(mappedBy = "storeId", cascade = CascadeType.ALL)  // remove all roles if store is removed
    private List<WorkRole> workRoles;

    public Store() {
    }

    public Store(String storeName, String streetName, String storeSuburb, int storePostcode, String storeCountry) {
        this.storeName = storeName;
        this.streetName = streetName;
        this.storeSuburb = storeSuburb;
        this.storePostcode = storePostcode;
        this.storeCountry = storeCountry;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getStoreSuburb() {
        return storeSuburb;
    }

    public void setStoreSuburb(String storeSuburb) {
        this.storeSuburb = storeSuburb;
    }

    public int getStorePostcode() {
        return storePostcode;
    }

    public void setStorePostcode(int storePostcode) {
        this.storePostcode = storePostcode;
    }

    public String getStoreCountry() {
        return storeCountry;
    }

    public void setStoreCountry(String storeCountry) {
        this.storeCountry = storeCountry;
    }

    public List<WorkRole> getWorkRoles() {
        return workRoles;
    }

    public void setWorkRoles(List<WorkRole> workRoles) {
        this.workRoles = workRoles;
    }
}
