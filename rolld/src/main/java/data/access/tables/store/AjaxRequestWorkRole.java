package data.access.tables.store;

import javax.persistence.*;

public class AjaxRequestWorkRole {

    private String roleName;

    private String roleComment;

    private String storeName;

    private boolean inTemplate;

    public AjaxRequestWorkRole() {
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleComment() {
        return roleComment;
    }

    public void setRoleComment(String roleComment) {
        this.roleComment = roleComment;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public boolean isInTemplate() {
        return inTemplate;
    }

    public void setInTemplate(boolean inTemplate) {
        this.inTemplate = inTemplate;
    }
}
