package data.access.tables.store;

import data.access.tables.user.User;

import javax.persistence.*;
import java.sql.Time;

@Entity
@Table(name="SHIFT")
public class Shift {

    @Id
    @Column(name = "shift_id")
    private int shiftId;

    @Column(name = "shift_day", nullable = false)
    private String shiftDay;

    @Column(name = "shift_start_time", nullable = false)
    private Time shiftStartTime;

    @Column(name = "shift_end_time", nullable = false)
    private Time shiftEndTime;

    @Column(name = "shift_comments")
    private String shiftComments;

    @Column(name = "shift_break_start")
    private Time shiftBreakStart;

    @Column(name = "shift_break_end")
    private String shiftBreakEnd;

    @ManyToOne(fetch= FetchType.EAGER, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @ManyToOne(fetch= FetchType.EAGER, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "roster_id", referencedColumnName = "roster_id")
    private Roster roster;

    public int getShiftId() {
        return shiftId;
    }

    public void setShiftId(int shiftId) {
        this.shiftId = shiftId;
    }

    public String getShiftDay() {
        return shiftDay;
    }

    public void setShiftDay(String shiftDay) {
        this.shiftDay = shiftDay;
    }

    public Time getShiftStartTime() {
        return shiftStartTime;
    }

    public void setShiftStartTime(Time shiftStartTime) {
        this.shiftStartTime = shiftStartTime;
    }

    public Time getShiftEndTime() {
        return shiftEndTime;
    }

    public void setShiftEndTime(Time shiftEndTime) {
        this.shiftEndTime = shiftEndTime;
    }

    public String getShiftComments() {
        return shiftComments;
    }

    public void setShiftComments(String shiftComments) {
        this.shiftComments = shiftComments;
    }

    public Time getShiftBreakStart() {
        return shiftBreakStart;
    }

    public void setShiftBreakStart(Time shiftBreakStart) {
        this.shiftBreakStart = shiftBreakStart;
    }

    public String getShiftBreakEnd() {
        return shiftBreakEnd;
    }

    public void setShiftBreakEnd(String shiftBreakEnd) {
        this.shiftBreakEnd = shiftBreakEnd;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Roster getRoster() {
        return roster;
    }

    public void setRoster(Roster roster) {
        this.roster = roster;
    }
}
