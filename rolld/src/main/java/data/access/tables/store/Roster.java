package data.access.tables.store;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="ROSTER")
public class Roster {

    @Id
    @Column(name = "roster_id")
    private int rosterId;

    @Column(name = "store_id", nullable = false)
    private int storeId;

    @Column(name = "roster_week_start_date", nullable = false)
    private Date rosterWeekStartDate;

    @MapsId("storeId")
    @ManyToOne(fetch= FetchType.EAGER, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "store_id")
    private Store store;

    @OneToMany(mappedBy = "shiftId", fetch= FetchType.EAGER, cascade = {CascadeType.ALL })
    private List<Shift> shiftList;

    public Roster() {}

    public Roster(int storeId, Date rosterWeekStartDate, Store store, List<Shift> shiftList){
        this.storeId = storeId;
        this.rosterWeekStartDate = rosterWeekStartDate;
        this.store = store;
        this.shiftList = shiftList;

    }

    public int getRosterId() {
        return rosterId;
    }

    public void setRosterId(int rosterId) {
        this.rosterId = rosterId;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public Date getRosterWeekStartDate() {
        return rosterWeekStartDate;
    }

    public void setRosterWeekStartDate(Date rosterWeekStartDate) {
        this.rosterWeekStartDate = rosterWeekStartDate;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public List<Shift> getShiftList() {
        return shiftList;
    }

    public void setShiftList(List<Shift> shiftList) {
        this.shiftList = shiftList;
    }

}

