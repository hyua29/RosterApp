package data.access.tables;

import data.access.tables.store.Store;

import javax.persistence.*;

@Entity
@Table(name = "TEMPLATE")
public class Template {

    @Id
    @Column(name = "user_role", nullable = false)
    private String userRole;

    @Column(name = "user_role_comments")
    private String userRoleComments;

    @Column(name = "store_id")
    private int storeId;

    @MapsId("storeId")
    @OneToOne(fetch= FetchType.EAGER, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "store_id", referencedColumnName = "store_id", nullable = false)
    private Store store;

    public Template(){}

    public Template(int storeId, String userRole, String userRoleComments) {
        this.storeId = storeId;
        this.userRole = userRole;
        this.userRoleComments = userRoleComments;

    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getUserRoleComments() {
        return userRoleComments;
    }

    public void setUserRoleComments(String userRoleComments) {
        this.userRoleComments = userRoleComments;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }


}
