package data.access.tables.userstoreauthority;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * define the composite id for UserStoreAuthority
 * There is no mapping at this stage, Hibernate uses equals() and hashcode() to determine whether two keys are the same;
 * hashcode must be unique for each composite key
 */
@Embeddable
public class UserStoreAuthorityPK implements Serializable {

    private int userId;

    private int storeId;

    private int authorityId;

    public UserStoreAuthorityPK() {}

    public UserStoreAuthorityPK(int userId, int storeId, int authorityId) {
        this.userId = userId;
        this.storeId = storeId;
        this.authorityId = authorityId;
    }

    @Override
    // https://stackoverflow.com/questions/1638723/how-should-equals-and-hashcode-be-implemented-when-using-jpa-and-hibernate
    // need to override equals() and hashcode() if the entity will be used in a Set
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserStoreAuthorityPK)) return false;
        UserStoreAuthorityPK that = (UserStoreAuthorityPK) o;
        return (getUserId() == that.getUserId()) &&
                (getStoreId() == that.getStoreId()) &&
                (getAuthorityId() == that.getAuthorityId());
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public int getAuthorityId() {
        return authorityId;
    }

    public void setAuthorityId(int authorityId) {
        this.authorityId = authorityId;
    }

}
