package data.access.tables.userstoreauthority;


import com.fasterxml.jackson.annotation.JsonView;
import data.access.tables.store.Store;
import data.access.tables.authority.UserAuthority;
import data.access.tables.user.User;
import helper.View;

import javax.persistence.*;

@Entity
@Table(name="USER_STORE_AUTHORITY")
public class UserStoreAuthority {

    // Override the mapping done by the Embeddable class
    @EmbeddedId
//    @AttributeOverrides({  // perform mapping for
//            @AttributeOverride( name = "userId", column = @Column(name = "user_id")),
//            @AttributeOverride(name = "storeId", column = @Column(name = "store_id")),
//            @AttributeOverride(name = "authorityId", column = @Column(name = "userauthority_id"))
//    })
    private UserStoreAuthorityPK id;

    @MapsId("userId")  // telling which attribute to use to perform the mapping
    @ManyToOne(fetch= FetchType.EAGER, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)  // use its own foreign key to join, define column to reference
    private User user;

    @MapsId("authorityId")
    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name = "userauthority_id", referencedColumnName = "userauthority_id", nullable = false)
    @JsonView(View.Public.class)
    private UserAuthority userAuthority;

    @MapsId("storeId")
    @ManyToOne(fetch= FetchType.EAGER, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "store_id", referencedColumnName = "store_id", nullable = false)
    private Store store;

    public UserStoreAuthority() {}

    public UserStoreAuthority(UserStoreAuthorityPK pK) {
        this.id = pK;
    }

    public UserStoreAuthorityPK getId() {
        return id;
    }

    public void setId(UserStoreAuthorityPK id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public UserAuthority getUserAuthority() {
        return userAuthority;
    }

    public void setUserAuthority(UserAuthority userAuthority) {
        this.userAuthority = userAuthority;
    }
}

