package exceptions;

import org.springframework.http.HttpStatus;

public enum ExceptionType {


    HTTP_INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "An internal server error occurred."),

    HIBERNATE_UPDATE_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to persist the object"),
    HIBERNATE_SEARCH_ERROR(HttpStatus.NOT_FOUND, "Failed to retrieve the object from database"),

    EMAIL_CONSTRUCT_ERROR("Failed to construct the email"),
    EMAIL_SEND_ERROR("Failed to send the email"),
    USER_MAIL_NOT_VALID("Email not valid"),

    POTENTIAL_EXPLOITATION("Unexpected action");

    //you can specify your own exception types...

    private HttpStatus status;
    private String message;

    ExceptionType(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }

    ExceptionType(String message) {
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

}
