package exceptions;

import helper.Log;

import java.text.MessageFormat;

public class ExceptionFactory {

    public static ApplicationSpecificException create(final Throwable cause, final ExceptionType exceptionType, final Object... messageArguments) {
        Log.getLogger().error(MessageFormat.format(exceptionType.getMessage(), messageArguments), cause);
        return new ApplicationSpecificException (exceptionType, cause, messageArguments);
    }

    public static ApplicationSpecificException create(final ExceptionType exceptionType, final Object... messageArguments) {
        Log.getLogger().error(MessageFormat.format(exceptionType.getMessage(), messageArguments));
        return new ApplicationSpecificException (exceptionType, messageArguments);
    }

//    public static ApplicationSpecificException create(final ExceptionType exceptionType, final Object... messageArguments) {
//        Log.getLogger().error(MessageFormat.format(exceptionType.getMessage(), messageArguments));
//        return new TerminologyServerException(exceptionType, messageArguments);
//    }

}
