package json.requests;


/**
 * Ajax request
 * TODO: consider to replace this with get request
 */
public class AjaxRequestWithEmail {

    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
