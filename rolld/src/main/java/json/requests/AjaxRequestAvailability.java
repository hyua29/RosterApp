package json.requests;

/**
 *
 * AJAX request for Availability entry.
 *
 */
public class AjaxRequestAvailability {
    private String newDate;
    private String availabilityStartTime;
    private String availabilityEndTime;


    public AjaxRequestAvailability() {
    }

    public String getNewDate() {
        return newDate;
    }

    public void setNewDate(String newDate) {
        this.newDate = newDate;
    }

    public String getAvailabilityStartTime() {
        return availabilityStartTime;
    }

    public void setAvailabilityStartTime(String availabilityStartTime) {
        this.availabilityStartTime = availabilityStartTime;
    }

    public String getAvailabilityEndTime() {
        return availabilityEndTime;
    }

    public void setAvailabilityEndTime(String availabilityEndTime) {
        this.availabilityEndTime = availabilityEndTime;
    }

    @Override
    public String toString() {
        return "AjaxRequestAvailability{" +
                "newDate='" + newDate + '\'' +
                ", availabilityStartTime='" + availabilityStartTime + '\'' +
                ", availabilityEndTime='" + availabilityEndTime + '\'' +
                '}';
    }
}
