package spike.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;


/**
 * aspect can intercept calls of other beans
 */
@Aspect
@Component
public class AopExample {

    @Before("execution(* run(..))")  // can use ||/&&/!
    public void log(JoinPoint joinPoint){
        System.out.println(joinPoint.getSignature().getName());
        System.out.println("\n ==> AOP is working !!!!!!!!!!!!!!!!");
    }

}
