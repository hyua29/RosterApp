package spring.config.routing;

import data.access.services.UserManagementService;
import data.access.tables.user.Availability;
import data.access.tables.user.User;
import helper.AjaxResponseBody;
import json.requests.AjaxRequestAvailability;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

// This is the logic controller
@Controller
@RequestMapping("/home")
public class MainPageController {

    @Autowired
    private UserManagementService userManagementService;

    @GetMapping("/")
    public String showMainPage1(HttpServletRequest httpServletRequest, Model model) {
        System.out.println("print user role--------------------------- ");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        System.out.println(authentication.getAuthorities());
        return "redirect: /home/my-schedule";
    }

    @GetMapping("")
    public String showMainPage2(HttpServletRequest httpServletRequest, Model model) {
        System.out.println("print user role--------------------------- ");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        System.out.println(authentication);
        System.out.println(authentication.getAuthorities());
        return "mySchedule";
    }

    @GetMapping("/my-schedule")
    public String showMainPage3(Authentication authentication, HttpServletRequest httpServletRequest, Model model) {
        System.out.println("print user role--------------------------- ");
        System.out.println(authentication.getAuthorities());
        return "mySchedule";
    }


    @PostMapping("/availabilities/availability-save")
    @ResponseBody  // return json
    public AjaxResponseBody userSaveAvailability(@RequestBody AjaxRequestAvailability obj) {
        System.out.println("Availability AJAX has been received!! "+obj);
        AjaxResponseBody<Availability> ajaxResponseBody = new AjaxResponseBody();

        Availability ava = new Availability("Tue", "1000", "1000");
        ava.setAvailabilityId(10);
        ajaxResponseBody.setResult(ava);
        return ajaxResponseBody;
    }

    /**
     *
     * @param session
     * @return All current user's availability
     */
    @GetMapping("/availabilities/user-availability")
    @ResponseBody  // return json
    public AjaxResponseBody userGetAvailability(HttpSession session) {
        String userEmail = (String) session.getAttribute("username");
        User currentUser = userManagementService.initAvailabilities(userManagementService.getUserByEmail(userEmail));
        System.out.println(currentUser);
        System.out.println(currentUser.getAvailability());
        return new AjaxResponseBody<Availability>(currentUser.getAvailability());
    }

    @GetMapping("/roster")
    public String showRosterPage() {
        return "rostering";
    }

    @GetMapping("/manage/employee")
    public String showManageEmployeePage() {
        return "manageEmployee";
    }


    @GetMapping("/edit")
    public String showEditProfile() {
        return "editProfile";
    }

    @GetMapping("/help")
    public String showHelpPage() {
        return "help";
    }

    //delete this later
    @GetMapping("/availabilities")
    public String showAvailabilitiesPage(){ return "availabilities"; }


//
//    System.out.println("printing user role: ");
//        System.out.println(authentication.getAuthorities());
//    HttpSession session = httpServletRequest.getSession();
//        System.out.println(session.getAttributeNames());
//
//    Enumeration<String> s = session.getAttributeNames();
//        while (s.hasMoreElements()) {
//        String value = s.nextElement();
//        System.out.println(value);
//        session.getAttribute(value);
//        System.out.println(session.getAttribute(value));
//    }
//
//        model.addAttribute("test", "testing");

}
