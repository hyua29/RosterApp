package spring.config.routing;

import
        data.access.services.NewUserInvitationService;

import data.access.services.ResetPasswordService;
import data.access.services.UserManagementService;
import data.access.tables.token.InvitationToken;
import data.access.tables.token.PasswordResetToken;
import exceptions.ApplicationSpecificException;
import helper.AjaxResponseBody;
import helper.Log;
import data.access.tables.user.UserDetailsByReceiver;
import data.access.tables.user.UserDetailsBySender;
import json.requests.AjaxRequestWithEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/authentication")
public class AuthenticationController {

    @Autowired
    private UserManagementService userManagementService;

    @Autowired
    private ResetPasswordService resetPasswordService;

    @Autowired
    private NewUserInvitationService newUserInvitationService;

    @RequestMapping("/login")
    public String ShowLoginPage() {
        return "loginPage";
    }

    /**
     * verify the token on the url
     * if genuine, save it to the session and redirect the user to the reset-page
     * display error message if invalid
     */
    @GetMapping("/verify-reset")
    public String verifyToken(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, HttpSession httpSession) {
        String resetToken = httpServletRequest.getParameter("reset-token");
        Log.getLogger().info("Verifying reset token: " + resetToken);
        if (resetPasswordService.verifyResetToken(resetToken)) {
            httpSession.setAttribute("reset-token", resetToken);
            Log.getLogger().info("token verified");
            return "redirect:" + "/authentication/password-reset";
        }

        // TODO: design a proper error page
        return "errorPage";
        //httpServletResponse.addCookie(new Cookie("test", "cookiessssssssssssssssss"));
    }

    /**
     * display an interface for the users to enter their email the reset url is sent to
     */
    @GetMapping("/forget-password")
    public String displayForgetPasswordPage() {
        return "forgetPassword";
    }

    /**
     * construct a unique url for the user to access to reset password
     *
     * @param ajaxRequestWithEmail
     * @return
     */
    @ResponseBody  // return json
    @PostMapping(value = "/send-password-token") // receive json as an object
    public AjaxResponseBody sendPasswordTokenToEmail(@RequestBody AjaxRequestWithEmail ajaxRequestWithEmail) {

        Log.getLogger().info("Json received: " + ajaxRequestWithEmail.getEmail());

        try {
            PasswordResetToken t = resetPasswordService.createPasswordResetTokenForUser(ajaxRequestWithEmail.getEmail());
            Log.getLogger().info("email exists");
            resetPasswordService.sendTokenToUser(ajaxRequestWithEmail.getEmail(), t.getToken());
        } catch (ApplicationSpecificException  e) {
            Log.getLogger().info("email not exists");
        }

        AjaxResponseBody result = new AjaxResponseBody();
        result.setMsg("okay");
        result.setCode("200");
        return result;
    }

    /**
     * provide an interface for the user to type in new password
     *
     * @param httpServletRequest
     * @param httpSession
     * @return
     */
    @PostMapping("/password-reset")
    public String displayChangePasswordPage(HttpServletRequest httpServletRequest, HttpSession httpSession) {
        Log.getLogger().info(httpSession.getAttribute("reset-token"));
        return "passwordReset";
    }

    /**
     * check whether two passwords are matching
     * update the new password to database
     * confirm the transaction to the user
     *
     * @return
     */
    @PostMapping("/save-new-password")
    public String saveNewPassword(HttpServletRequest httpServletRequest, HttpSession httpSession) {
        String newPassword = httpServletRequest.getParameter("new-password");
        String newPasswordConfirm = httpServletRequest.getParameter("new-password-confirm");
        Log.getLogger().info("new password:" + newPassword);
        Log.getLogger().info("confirm: " + newPassword.equals(newPasswordConfirm));
        if (resetPasswordService.resetPassword(newPassword, newPasswordConfirm, (String) httpSession.getAttribute("reset-token")))
            return "redirect:" + "/authentication/confirm-save-password?reset-success=true";
        else
            return "redirect:" + "/authentication/confirm-save-password?reset-success=false";
    }

    @GetMapping("/confirm-save-password")
    public String confirmSavePassword(HttpServletRequest httpServletRequest, Model model) {
        if (httpServletRequest.getParameter("reset-success").equals("true"))
            model.addAttribute("message", "New Password Saved");
        else
            model.addAttribute("message", "Failed to Update Password");
        return "transactionConfirm";
    }

    @RequestMapping("/error")
    public String ShowErrorPage() {
        return "errorPage";
    }

    /**
     * TODO: validate the token and determine the role
     * TODO: provide enough details to the sign in page (to session maybe)
     * @param httpRequest
     * @return
     */
    @GetMapping("/invitation-verification")
    public String confirmInvitation(@RequestParam("invitation-token") String token, HttpServletRequest httpRequest, HttpSession session) {
        if (!newUserInvitationService.verifyResetToken(token))
            return "errorPage";
        Log.getLogger().info("Invitation Token Verified: " + httpRequest.getParameter("invitation-token"));


        session.setAttribute("invitation-token", token);
        return "redirect:" + "/authentication/sign-up";
    }

    /**
     * Return a sign up page to user with valid token
     *
     * @return
     */
    @GetMapping("/sign-up")
    public String displaySignUpPage() {
        return "signUpPage";
    }

    /**
     * get user details and put the info provided by the user
     * together with the information linked to the invitation token
     * the create user account
     *
     * @return
     */
    @PostMapping("/sign-up-confirm")
    public String confirmSighUp(@ModelAttribute("user-details") UserDetailsByReceiver userDetailsByReceiver, HttpSession session, Model model) {
        Log.getLogger().info(userDetailsByReceiver.toString());
        Log.getLogger().info(session.getAttribute("invitation-token"));
        if(session.getAttribute("invitation-token") == null)
            session.setAttribute("invitation-token", "");

        if (newUserInvitationService.createNewUser(userDetailsByReceiver, (String) session.getAttribute("invitation-token"))) {
            model.addAttribute("message", "New user created");

        }
        else
            model.addAttribute("message", "Failed to create new user");

        return "transactionConfirm";
    }

    /**
     * for the manager to invite new user
     * receive new user's email through ajax and send an email with unique sign up tokens
     * @return
     */ 
    @ResponseBody  // return json
    @PostMapping(value = "/new-user-invite") // receive json as an object
    public AjaxResponseBody inviteNewUser(@RequestBody UserDetailsBySender userDetailsBySender) {

        Log.getLogger().info("Json received: " + userDetailsBySender);
        //TODO: check the store the manager is in charge of from the authentication token
        InvitationToken i = newUserInvitationService.createTokenForNewUser(userDetailsBySender);  // create the token and save it

        if (i != null) { // send email if store id and authority id are correct
            Log.getLogger().info("input verified");
            newUserInvitationService.sendInvitationToUser(userDetailsBySender.getEmail(), i.getInvitationToken());
        } else
            Log.getLogger().info("email not exists");

        AjaxResponseBody result = new AjaxResponseBody();
        result.setMsg("okay");
        result.setCode("200");

        return result;
    }


    /**
     * This creates a new address object for the empty form and stuffs it into the model
     * @return
     */
    @ModelAttribute("user-details")
    public UserDetailsByReceiver populateUser() {
        UserDetailsByReceiver userDetailsByReceiver = new UserDetailsByReceiver();

        return userDetailsByReceiver;
    }
}
