package spring.config.routing;

import data.access.services.StoreManagementService;
import data.access.services.UserManagementService;
import data.access.tables.store.AjaxRequestWorkRole;
import data.access.tables.store.WorkRole;
import data.access.tables.user.User;
import exceptions.ExceptionFactory;
import helper.AjaxResponseBody;
import helper.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

import static exceptions.ExceptionType.POTENTIAL_EXPLOITATION;


@Controller
@RequestMapping("/store-manager")
public class ManagerController {

    @Autowired
    private UserManagementService userManagementService;

    @Autowired
    private StoreManagementService storeManagementService;

    /**
     * get the store the manager/owner is in charge of, put the store names into session and display it to the page
     * @param model
     * @param session
     * @return
     */
    @GetMapping("/{pageType}")
    //@PreAuthorize("hasAnyRole('MANAGER', 'OWNER')")
    public String showManageEmployeePage(@PathVariable String pageType, Model model, HttpSession session) {
        model.addAttribute("storeNames", storeManagementService.getStoreNames((String) session.getAttribute("username")));
        List<String> storeInCharge =  storeManagementService.getStoreNames((String) session.getAttribute("username"));
        session.setAttribute("store-in-charge", storeInCharge);

        if(pageType.equals("employee"))
            return "manageEmployee";
        else if(pageType.equals("store"))
            return "manageStore";
        else if(pageType.equals("roster"))
            return "rostering";
        else
            return "errorPage";
    }
//
//    @GetMapping("/store")
//    public String showManageStorePage(Model model, HttpSession session) {
//        model.addAttribute("storeNames", storeManagementService.getStoreNames((String) session.getAttribute("username")));
//        List<String> storeInCharge =  storeManagementService.getStoreNames((String) session.getAttribute("username"));
//        session.setAttribute("store-in-charge", storeInCharge);
//        return "manageStore";
//    }

    /**
     * Get the store name from the user and compare it with the store names saved in the session
     * This method must be called after 'showManageEmployeePage'
     * @param storeName
     * @param session
     * @return
     */
    @GetMapping("/current-store-employee-details")
    @ResponseBody
    //@PreAuthorize("hasAnyRole('MANAGER', 'OWNER')")
    public AjaxResponseBody<User> returnEmployeeDetails(@RequestParam("store-name") String storeName, HttpSession session) {
        AjaxResponseBody<User> responseBody = new AjaxResponseBody<User>();

        Log.getLogger().warn("-----------" + storeName);
        List<String> storeInCharge = (List<String>) session.getAttribute("store-in-charge");

        if(storeInCharge != null && storeInCharge.contains(storeName)) {
            responseBody.setResultMap(storeManagementService.getEmployeeDetailsFromCurrentStore(storeName));
        } else
            throw ExceptionFactory.create(POTENTIAL_EXPLOITATION);  // this wont happen if the user browse the website in a expected sequence

        return responseBody;
    }

    /**
     * Get the store name from the user and compare it with the store names saved in the session
     * This method must be called after 'showManageEmployeePage'
     * @param storeName
     * @param session
     * @return
     */
    @GetMapping("/roster/work-roles")
    @ResponseBody
    public AjaxResponseBody<WorkRole> returnWorkRoles(@RequestParam("store-name") String storeName, HttpSession session) {
        AjaxResponseBody<WorkRole> responseBody = new AjaxResponseBody<WorkRole>();

        List<String> storeInCharge = (List<String>) session.getAttribute("store-in-charge");

        if(storeInCharge != null && storeInCharge.contains(storeName)) {
            Log.getLogger().warn("-----------" + "Store Name valid");
            responseBody.setResultList(storeManagementService.getWorkRoleByStoreName(storeName));
        } else
                   throw ExceptionFactory.create(POTENTIAL_EXPLOITATION);  // this wont happen if the user browse the website in a expected sequence

        return responseBody;
    }

    /**
     * Save a new work role to the database
     * This method must be called after 'showManageEmployeePage'
     * @param requestWorkRole
     * @param session
     * @return
     */
    @PostMapping("/roster/work-role-save")
    @ResponseBody
    public AjaxResponseBody<WorkRole> saveNewWorkRole(@RequestBody AjaxRequestWorkRole requestWorkRole, HttpSession session) {
        AjaxResponseBody<WorkRole> responseBody = new AjaxResponseBody<WorkRole>();
        List<String> storeInCharge = (List<String>) session.getAttribute("store-in-charge");
        if(storeInCharge != null && storeInCharge.contains(requestWorkRole.getStoreName())) {
            Log.getLogger().warn("-----------" + "Store Name valid");
            responseBody.setResult(storeManagementService.createWorkRole(requestWorkRole));
        } else
            throw ExceptionFactory.create(POTENTIAL_EXPLOITATION);
        return responseBody;
    }



}
