package helper;

/**
 *
 * This class is used to create user availability object
 *
 */
public class UserAvailability {
    private int availabilityId;
    private int userId;
    private String availabilityDay;
    private String availabilityStartTime;
    private String availabilityEndTime;

    /**
     * Constructor method
     * @param userId
     * @param availabilityDay
     * @param availabilityStartTime
     * @param availabilityEndTime
     */
    public UserAvailability(int userId, String availabilityDay, String availabilityStartTime, String availabilityEndTime){
        this.userId = userId;
        this.availabilityDay = availabilityDay;
        this.availabilityStartTime = availabilityStartTime;
        this.availabilityEndTime = availabilityEndTime;
    }

    public UserAvailability(){
    }

    public int getAvailabilityId() {
        return availabilityId;
    }

    public void setAvailabilityId(int availabilityId) {
        this.availabilityId = availabilityId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getAvailabilityDay() {
        return availabilityDay;
    }

    public void setAvailabilityDay(String availabilityDay) {
        this.availabilityDay = availabilityDay;
    }

    public String getAvailabilityStartTime() {
        return availabilityStartTime;
    }

    public void setAvailabilityStartTime(String availabilityStartTime) {
        this.availabilityStartTime = availabilityStartTime;
    }

    public String getAvailabilityEndTime() {
        return availabilityEndTime;
    }

    public void setAvailabilityEndTime(String availabilityEndTime) {
        this.availabilityEndTime = availabilityEndTime;
    }

    @Override
    public String toString(){
        return "UserAvailability{" +
                "availabilityId='" + availabilityId + '\'' +
                ", userId='" + userId + '\'' +
                ", availabilityDay='" + availabilityDay + '\'' +
                ", availabilityStartTime='" + availabilityStartTime + '\'' +
                ", availabilityEndTime='" + availabilityEndTime + '\'' +
                '}';
    }
}