package helper;

import com.fasterxml.jackson.annotation.JsonView;

import java.lang.reflect.Array;
import java.util.List;
import java.util.Map;

public class AjaxResponseBody<T> {

    @JsonView(View.Public.class)
    private String msg;

    @JsonView(View.Public.class)
    private String code;

    @JsonView(View.Public.class)
    private Map<String, T> resultMap;

    @JsonView({View.Public.class})
    private List<T> resultList;

    @JsonView({View.Public.class})
    private T[] resultArray;

    @JsonView({View.Public.class})
    private T result;

    public AjaxResponseBody() {
        this.msg = "okay";
        this.code = "200";
    }

    public AjaxResponseBody(List<T> resultList) {
        this.msg = "okay";
        this.code = "200";
        this.resultList = resultList;
    }

    public AjaxResponseBody(Map<String, T> resultMap) {
        this.msg = "okay";
        this.code = "200";
        this.resultMap = resultMap;
    }

    public AjaxResponseBody(T[] resultArray) {
        this.msg = "okay";
        this.code = "200";
        this.resultArray = resultArray;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Map<String, T> getResultMap() {
        return resultMap;
    }

    public void setResultMap(Map<String, T> resultMap) {
        this.resultMap = resultMap;
    }

    public List<T> getResultList() {
        return resultList;
    }

    public void setResultList(List<T> resultList) {
        this.resultList = resultList;
    }

    public T[] getResultArray() {
        return resultArray;
    }

    public void setResultArray(T[] resultArray) {
        this.resultArray = resultArray;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }
}