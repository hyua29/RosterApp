package helper;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * Enable global access to log4J
 */
@Component
public class Log {

    private static final Logger logger = Logger.getLogger(Log.class);

    public static Logger getLogger() {
        return logger;
    }

}
