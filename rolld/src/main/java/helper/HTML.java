package helper;

public class HTML {

    /**
     * Helps construct Hyper link
     * @param displayedContent
     * @param actualURL
     * @return
     */
    static public String constructHyperLink(String displayedContent, String actualURL) {
        return "<a href=" +"'" + actualURL + "'" + ">" + displayedContent + "</a>";
    }
}
