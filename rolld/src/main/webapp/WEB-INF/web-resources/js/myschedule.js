var timetable = new Timetable();

timetable.setScope(5,22);

timetable.addLocations(['Pho + Fryer', 'Summary', 'POS 1', 'POS 2', 'POS 3', 'Jakarta', 'Tokyo']);
timetable.addEvent('Jane Doe | 5.5 Hours', 'Pho + Fryer', new Date(2015,7,17,9,00), new Date(2015,7,17,14,30), { onClick: function(){
        $("#shiftModal").modal()
    }});
timetable.addEvent('John Doe', 'Summary', new Date(2015,7,17,12), new Date(2015,7,17,13), { url: '#' });
timetable.addEvent('John Doe', 'Summary', new Date(2015,7,17,9,30), new Date(2015,7,17,15), { url: '#' });
timetable.addEvent('Lasergaming', 'POS 2', new Date(2015,7,17,9,45), new Date(2015,7,17,14,30), { class: 'vip-only', data: { maxPlayers: 14, gameType: 'Capture the flag' } });
timetable.addEvent('All-you-can-eat grill', 'POS 3', new Date(2015,7,17,18), new Date(2015,7,17,20,30), { url: '#' });
timetable.addEvent('Tokyo Hackathon Livestream', 'POS 1', new Date(2015,7,17,12,30), new Date(2015,7,17,16,15)); // options attribute is not used for this event
timetable.addEvent('Lunch', 'Jakarta', new Date(2015,7,17,9,30), new Date(2015,7,17,11,45), { onClick: function(event) {
        window.alert('You clicked on the ' + event.name + ' event in ' + event.location + '. This is an example of a click handler');
    }});
var renderer = new Timetable.Renderer(timetable);
renderer.draw('.timetable');