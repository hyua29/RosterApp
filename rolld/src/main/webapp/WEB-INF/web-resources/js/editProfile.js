function showEditPic() {
    var editPic = document.getElementById("editPic");
    editPic.style.display = "block";
}

function hideEditPic() {
    var editPic = document.getElementById("editPic");
    editPic.style.display = "none";
}

function fasterPreview( uploader ) {
    if ( uploader.files && uploader.files[0] ){
        $('#profilePic').attr('src',
            window.URL.createObjectURL(uploader.files[0]) );
    }
}

function uploadPic() {
    var uploadPic = document.getElementById('uploadPic');
    uploadPic.click();
    $("#uploadPic").change(function(){
        fasterPreview( this );
    });

}