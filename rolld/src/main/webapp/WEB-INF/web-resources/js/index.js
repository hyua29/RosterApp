generateNavBar();

(function($) {
  "use strict"; // Start of use strict

  // Configure tooltips for collapsed side navigation
  $('.navbar-sidenav [data-toggle="tooltip"]').tooltip({
    template: '<div class="tooltip navbar-sidenav-tooltip" role="tooltip" style="pointer-events: none;"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
  })

  // Toggle the side navigation
  $("#sidenavToggler").click(function(e) {
    e.preventDefault();
    $("body").toggleClass("sidenav-toggled");
    $(".navbar-sidenav .nav-link-collapse").addClass("collapsed");
    $(".navbar-sidenav .sidenav-second-level, .navbar-sidenav .sidenav-third-level").removeClass("show");
  });

  // Force the toggled class to be removed when a collapsible nav link is clicked
  $(".navbar-sidenav .nav-link-collapse").click(function(e) {
    e.preventDefault();
    $("body").removeClass("sidenav-toggled");
  });

  // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
  $('body.fixed-nav .navbar-sidenav, body.fixed-nav .sidenav-toggler, body.fixed-nav .navbar-collapse').on('mousewheel DOMMouseScroll', function(e) {
    var e0 = e.originalEvent,
      delta = e0.wheelDelta || -e0.detail;
    this.scrollTop += (delta < 0 ? 1 : -1) * 30;
    e.preventDefault();
  });

  // Scroll to top button appear
  $(document).scroll(function() {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  });

  // Configure tooltips globally
  $('[data-toggle="tooltip"]').tooltip()

  // Smooth scrolling using jQuery easing
  $(document).on('click', 'a.scroll-to-top', function(event) {
    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: ($($anchor.attr('href')).offset().top)
    }, 1000, 'easeInOutExpo');
    event.preventDefault();
  });
})(jQuery); // End of use strict



function generateNavBar(){

    console.log("hey");
    var schedule = ""; var availabilities=""; var roster = ""; var manageEmployees = ""; var manageStores = ""; var editProfile = ""; var help = ""; var manageShow = "";

    //TODO: Create selection to decide which class is currently active:
    //      Then, copy this '+ classActive +'

    if(window.location.pathname.includes("availabilities")) {
        manageShow = "";
        availabilities = "active";
    } else if(window.location.pathname.includes("roster")) {
        manageShow = "";
        roster = "active";
    } else if(window.location.pathname.includes("employee")) {
        manageShow = "show";
        manageEmployees = "active";
    } else if(window.location.pathname.includes("store")) {
        manageShow = "show";
        manageStores = "active";
    } else if(window.location.pathname.includes("edit")) {
        manageShow = "";
        editProfile = "active";
    } else if(window.location.pathname.includes("help")) {
        manageShow = "";
        help = "active";
    } else if(window.location.pathname.includes("home")) {
        manageShow = "";
        schedule = "active";
    }

        var myvar =
        '    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">'+
        '        <span class="navbar-toggler-icon"></span>'+
        '    </button>'+
        '    <div class="collapse navbar-collapse" id="navbarResponsive">'+
        '        <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">'+
        ''+
        //Schedule page
        '            <li class="nav-item '+ schedule +'" data-toggle="tooltip" data-placement="right" title="MySchedule">'+
        '                <a class="nav-link " href="/home">'+
        '                    <i class="fa fa-fw fa-user"></i>'+
        '                    <span class="nav-link-text">My Schedule</span>'+
        '                </a>'+
        '            </li>'+
        ''+
        '            <!-- Availabilities -->'+
        '            <li class="nav-item '+ availabilities +'" data-toggle="tooltip" data-placement="right" title="Availabilities">'+
        '                <a class="nav-link " href="/home/availabilities">'+
        '                    <i class="fa fa-fw fa-calendar"></i>'+
        '                    <span class="nav-link-text">Availabilities</span>'+
        '                </a>'+
        '            </li>'+
        ''+
        '            <!-- Rostering -->'+
        '            <li class="nav-item '+ roster +'" data-toggle="tooltip" data-placement="right" title="Rostering">'+
        '                <a class="nav-link " href="/home/roster">'+
        '                    <i class="fa fa-fw fa-calendar-check-o"></i>'+
        '                    <span class="nav-link-text">Rostering</span>'+
        '                </a>'+
        '            </li>'+
        ''+
        '            <!-- Managing -->'+
        '            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Managing">'+
        '                <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion" id="manageAccordion">'+
        '                    <i class="fa fa-fw fa-cogs"></i>'+
        '                    <span class="nav-link-text">Managing</span>'+
        '                </a>'+
        '                <ul class="sidenav-second-level collapse '+ manageShow +'" id="collapseComponents">'+
        '                    <li class='+ manageEmployees +'>'+
        '                        <a href="/store-manager/employee"><i class="fa fa-users"></i> Manage Employees</a>'+
        '                    </li>'+
        '                    <li class= '+ manageStores +'>'+
        '                        <a href="/store-manager/store"><i class="fa fa-home"></i> Manage Stores</a>'+
        '                    </li>'+
        '                </ul>'+
        '            </li>'+
        '            <!-- Edit Profile -->'+
        '            <li class="nav-item '+ editProfile +'" data-toggle="tooltip" data-placement="right" title="Edit Profile">'+
        '                <a class="nav-link" href="/home/edit">'+
        '                    <i class="fa fa-fw fa-edit"></i>'+
        '                    <span class="nav-link-text">Edit Profile</span>'+
        '                </a>'+
        '            </li>'+
        ''+
        '            <li class="nav-item '+ help +'" data-toggle="tooltip" data-placement="right" title="Help">'+
        '                <a class="nav-link" href="/home/help">'+
        '                    <i class="fa fa-fw fa-question"></i>'+
        '                    <span class="nav-link-text">Help</span>'+
        '                </a>'+
        '            </li>'+
        '        </ul>'+
        '        <ul class="navbar-nav sidenav-toggler">'+
        '            <li class="nav-item">'+
        '                <a class="nav-link text-center" id="sidenavToggler">'+
        '                    <i class="fa fa-fw fa-angle-left"></i>'+
        '                </a>'+
        '            </li>'+
        '        </ul>'+
        '        <ul class="navbar-nav ml-auto">'+
        '            <li class="nav-item">'+
        '                <form class="form-inline my-2 my-lg-0 mr-lg-2">'+
        '                    <div class="input-group">'+
        '                        <input class="form-control" type="text" placeholder="Search for...">'+
        '                        <span class="input-group-append">'+
        '                <button class="btn btn-warning" type="button">'+
        '                  <i class="fa fa-search"></i>'+
        '                </button>'+
        '              </span>'+
        '                    </div>'+
        '                </form>'+
        '            </li>'+
        '            <li class="nav-item">'+
        '                <a class="nav-link" data-toggle="modal" data-target="#exampleModal">'+
        '                    <i class="fa fa-fw fa-sign-out"></i>Logout</a>'+
        '            </li>'+
        '        </ul>'+
        '    </div>';


    var mainNav = document.getElementById('mainNav');
    mainNav.innerHTML += myvar;

    var element = document.getElementById('manageAccordion');
    element.classList.remove("collapsed");
}

