


function reset() {
    document.getElementById('#clearField').reset();
}


//Bad practice
var textData = [];

function validateForm() {

    var date = document.forms["addExtra"]["newDate"].value;
    var start = document.forms["addExtra"]["newStartInput"].value;
    var end = document.forms["addExtra"]["newEndInput"].value;

    var counter = textData.length;
    //Instead of doing this, try to make it into an array and render it later on.

    var displayData = document.getElementById("additionalHoursData");
    var newString = "new" + counter;

    if($('#additionalHoursData').has("small")){
        // When there is a small tag, remove it and replace with the new div
        var formatAdded = '<div class="form-group row" id="new'+ counter + '">'+
            '                        <label for="addedStart" class="col-4 col-form-label">'+ date +'</label>'+
            '                        <div class="col-3">'+
            '                            <div class="form-group">'+
            '                                <div class="input-group date" id="addedStart'+ counter +'" data-target-input="nearest">'+
            '                                    <input type="text" class="form-control datetimepicker-input" value="'+ start+'" data-target="#addedStart'+ counter +'"'+
            '                                           placeholder="Start"/>'+
            '                                    <div class="input-group-append" data-target="#addedStart'+ counter +'"'+
            '                                         data-toggle="datetimepicker">'+
            '                                        <div class="input-group-text"><i class="fa fa-clock-o"></i></div>'+
            '                                    </div>'+
            '                                </div>'+
            '                            </div>'+
            '                        </div>'+
            '                        <div class="col-3">'+
            '                            <div class="form-group">'+
            '                                <div class="input-group date" id="addedEnd'+ counter +'" data-target-input="nearest">'+
            '                                    <input type="text" class="form-control datetimepicker-input" value="'+ end +'" data-target="#addedEnd'+ counter +'"'+
            '                                           placeholder="End"/>'+
            '                                    <div class="input-group-append" data-target="#addedEnd'+ counter +'" data-toggle="datetimepicker">'+
            '                                        <div class="input-group-text"><i class="fa fa-clock-o"></i></div>'+
            '                                    </div>'+
            '                                </div>'+
            '                            </div>'+
            '                        </div>'+
            '                       <div class="col-2">' +
            '                           <button id="'+newString+'Button" class="btn btn-sm btn-danger" onclick="removeSpecificAvailabilities(this)">Remove</button> ' +
            '                       </div>'+

            '                    </div>';
        // Remove button doesnt work as expected if includes the "onclick" tag inside. My assumption is it relates with
        // The backend system where either you can delete that and refresh the page every time. If it does work with
        // frontend, then use Jquery .remove() to remove respected div


        textData.push(formatAdded);

        var finalHTML = "";
        if (textData.length !== 0) {
            for (i = 0; i < textData.length; i++) {
                finalHTML += textData[i];
            }
        }

        displayData.innerHTML = finalHTML
        $(function (){
            $('[id^="addedStart"]' ).datetimepicker({
                format: 'LT'
            });

            $('[id^="addedEnd"]' ).datetimepicker({
                format: 'LT'
            });
            console.log("called");
        });

    }
}

//LT is from the API

$(function () {

    $('#newStart').datetimepicker({
        format: 'LT'
    });

    $('#newEnd').datetimepicker({
        format: 'LT'
    });


});

function removeSpecificAvailabilities(theId){
    var displayData = document.getElementById("additionalHoursData");
    var str = theId.id;
    str = str.slice(0, -6);

    var numb = str;
    numb = numb.substr(3);

    var numbInt = parseInt(numb);

    $(function(){
        $('#'+str).remove();
    });

    //Removing from global array
    if(textData.length > 1){
        textData.splice(numbInt,1);
    }
    else{
        textData.shift();
    }



   //Have to fix this...

    if(textData.length === 0){
        var defaultText = "<small id=\"emptyExtra\" class=\"text-muted\">Nothing to show at the moment. <br> If you wish to add more availability,\n" +
            "                            please click \"Add More Availabilities\" button below.</small>";
        displayData.innerHTML = defaultText;
    }

    console.log(textData)
}

function convertTimeFrom12To24(timeStr) {
    var colon = timeStr.indexOf(':');
    var hours = timeStr.substr(0, colon),
        minutes = timeStr.substr(colon + 1, 2),
        meridian = timeStr.substr(colon + 4, 2).toUpperCase();


    var hoursInt = parseInt(hours, 10),
        offset = meridian == 'PM' ? 12 : 0;

    if (hoursInt === 12) {
        hoursInt = offset;
    } else {
        hoursInt += offset;
    }
    return hoursInt + ":" + minutes;
}




$(".monday").on("input", function () {
        //console.log("Change to " + this.value);
        var startTime = $('#mondayInputStart').val();

        var endTime = $('#mondayInputEnd').val();

        console.log(startTime);
        console.log(endTime);
        var arbitraryDate = '01/01/2011';
        if(startTime && endTime) {
            if (Date.parse(arbitraryDate + ' ' + convertTimeFrom12To24(startTime)) >= Date.parse(arbitraryDate + ' ' + convertTimeFrom12To24(endTime))) {
                //console.log('error');
                $('#mondayErrorLabel').css('display', 'block');
            } else {
                $('#mondayErrorLabel').css('display', 'none');
            }
        }
    }
);

$(".tuesday").on("input", function () {
        //console.log("Change to " + this.value);
        var startTime = $('#tuesdayInputStart').val();

        var endTime = $('#tuesdayInputEnd').val();

        console.log(startTime);
        console.log(endTime);
        var arbitraryDate = '01/01/2011';
        if(startTime && endTime) {
            if (Date.parse(arbitraryDate + ' ' + convertTimeFrom12To24(startTime)) >= Date.parse(arbitraryDate + ' ' + convertTimeFrom12To24(endTime))) {
                //console.log('error');
                $('#tuesdayErrorLabel').css('display', 'block');
            } else {
                $('#tuesdayErrorLabel').css('display', 'none');
            }
        }
    }
);

$(".wednesday").on("input", function () {
        //console.log("Change to " + this.value);
        var startTime = $('#wednesdayInputStart').val();

        var endTime = $('#wednesdayInputEnd').val();

        console.log(startTime);
        console.log(endTime);
        var arbitraryDate = '01/01/2011';
        if(startTime && endTime) {
            if (Date.parse(arbitraryDate + ' ' + convertTimeFrom12To24(startTime)) >= Date.parse(arbitraryDate + ' ' + convertTimeFrom12To24(endTime))) {
                //console.log('error');
                $('#wednesdayErrorLabel').css('display', 'block');
            } else {
                $('#wednesdayErrorLabel').css('display', 'none');
            }
        }
    }
);

$(".thursday").on("input", function () {
        //console.log("Change to " + this.value);
        var startTime = $('#thursdayInputStart').val();

        var endTime = $('#thursdayInputEnd').val();

        console.log(startTime);
        console.log(endTime);
        var arbitraryDate = '01/01/2011';
        if(startTime && endTime) {
            if (Date.parse(arbitraryDate + ' ' + convertTimeFrom12To24(startTime)) >= Date.parse(arbitraryDate + ' ' + convertTimeFrom12To24(endTime))) {
                //console.log('error');
                $('#thursdayErrorLabel').css('display', 'block');
            } else {
                $('#thursdayErrorLabel').css('display', 'none');
            }
        }
    }
);

$(".friday").on("input", function () {
        //console.log("Change to " + this.value);
        var startTime = $('#fridayInputStart').val();

        var endTime = $('#fridayInputEnd').val();

        console.log(startTime);
        console.log(endTime);
        var arbitraryDate = '01/01/2011';
        if(startTime && endTime) {
            if (Date.parse(arbitraryDate + ' ' + convertTimeFrom12To24(startTime)) >= Date.parse(arbitraryDate + ' ' + convertTimeFrom12To24(endTime))) {
                //console.log('error');
                $('#fridayErrorLabel').css('display', 'block');
            } else {
                $('#fridayErrorLabel').css('display', 'none');
            }
        }
    }
);

$(".saturday").on("input", function () {
        //console.log("Change to " + this.value);
        var startTime = $('#saturdayInputStart').val();

        var endTime = $('#saturdayInputEnd').val();

        console.log(startTime);
        console.log(endTime);
        var arbitraryDate = '01/01/2011';
        if(startTime && endTime) {
            if (Date.parse(arbitraryDate + ' ' + convertTimeFrom12To24(startTime)) >= Date.parse(arbitraryDate + ' ' + convertTimeFrom12To24(endTime))) {
                //console.log('error');
                $('#saturdayErrorLabel').css('display', 'block');
            } else {
                $('#saturdayErrorLabel').css('display', 'none');
            }
        }
    }
);


$(".sunday").on("input", function () {
        //console.log("Change to " + this.value);
        var startTime = $('#sundayInputStart').val();

        var endTime = $('#sundayInputEnd').val();

        console.log(startTime);
        console.log(endTime);
        var arbitraryDate = '01/01/2011';
        if(startTime && endTime) {
            if (Date.parse(arbitraryDate + ' ' + convertTimeFrom12To24(startTime)) >= Date.parse(arbitraryDate + ' ' + convertTimeFrom12To24(endTime))) {
                //console.log('error');
                $('#sundayErrorLabel').css('display', 'block');
            } else {
                $('#sundayErrorLabel').css('display', 'none');
            }
        }
    }
);

//
// //angular js and ajax
// var app = angular.module('availability', []);
// app.run(function($rootScope) {
//     $rootScope.csrfHeader = 0;
//     $rootScope.csrfToken = 0;
//     $rootScope.init = function () {
//         $rootScope.csrfHeader = document.head.querySelector("[name=_csrf_header]").content;
//         $rootScope.csrfToken = document.head.querySelector("[name=_csrf]").content;
//     };
// });
//
// app.controller('AvailabilityController', function($scope, $rootScope) {
//     $scope.availabilities = [];
//
//     $scope.loadAva =  (function() {
//         $.ajax({
//             type: "GET",
//             contentType: "application/json",
//             url: _contextPath + "/home/availabilities/user-availability",  //FIXME: Domain name is hardcoded
//             dataType: 'json',
//             timeOut: 10000,
//             beforeSend: function(request) {  // append csrf token to request header
//                 request.setRequestHeader($scope.csrfHeader, $scope.csrfToken);
//             },
//
//             success: function (data) {
//                 console.log("ajax succeeded");
//                 $scope.$apply($scope.availabilities = data.resultList); // apply changes once data has been retrieved
//                 console.log($scope.availabilities);
//             },
//             error: function (e) {
//                 console.log("ajax failed");
//                 console.log(e);
//             },
//             done: function (e) {
//                 console.log("ajax finished. availability entries created");
//             }
//         });
//
//     });
//
//     $scope.addAva = function () {
//             console.log("ajax request sent");
//             var requestJSON = {newDate: $("#newDate").val(), availabilityStartTime: $("#newStartInput").val(), availabilityEndTime: $("#newEndInput").val() };  // data sent to the server
//             console.log(requestJSON);
//             console.log(JSON.stringify(requestJSON));
//             $.ajax({
//                 type: "POST",
//                 contentType: "application/json",
//                 url: _contextPath + "/home/availabilities/availability-save",  //FIXME: Domain name is hardcoded
//                 data: JSON.stringify(requestJSON),
//                 dataType: 'json',
//                 timeOut: 10000,
//                 beforeSend: function(request) {  // append csrf token to request header
//                     request.setRequestHeader($scope.csrfHeader, $scope.csrfToken);
//                 },
//
//                 success: function (data) {
//                     console.log("ajax succeeded");
//                     $scope.apply($scope.availabilities.push(data));
//                     console.log($scope.availabilities);
//                 },
//                 error: function (e) {
//                     console.log("ajax failed");
//                     console.log(e);
//                 },
//                 done: function (e) {
//                     console.log("ajax finished. availability entries created");
//                 }
//             });
//     }
// });


