
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Zest Developer">

    <title>RosterApp</title>
    <!-- Bootstrap core CSS-->
    <link href="${pageContext.request.contextPath}/resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="${pageContext.request.contextPath}/resources/vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template-->
    <link href="${pageContext.request.contextPath}/resources/css/index.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/interiorpage.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/timetablejs.css" rel="stylesheet">

    <!-- Logo -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="mobile-web-app-capable" content="yes" />
    <link rel="shortcut icon" sizes="196x196" href="${pageContext.request.contextPath}/resources/img/logo.png" />
    <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/resources/img/logo.png" />
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a href="/home"><img src="${pageContext.request.contextPath}/resources/img/roster_i.png" width="150px" class="navbar-brand" href="*"></a>
</nav>

<!-- Content -->
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <!-- Left panel-->
            <div class="col-lg-4 mb-1 ">
                <div class="card mb-3">
                    <div class="card-body text-center container-fluid">
                        <img alt="Profile Picture" width="150px" class="img-circle" src="${pageContext.request.contextPath}/resources/img/person.jpg">
                        <h1>Jane Doe</h1>
                        <p class="text-muted">Manager</p>
                        <p class="font-weight-bold">Status: Full Time</p>
                        <div class="row">
                            <div class="col-lg-4 mb-2">
                                <label>Payrate</label>
                                <h3 class="text-primary">$25.1</h3>
                                <label>per hour</label>
                            </div>
                            <div class="col-lg-4 mb-2">
                                <label>This Week</label>
                                <h4>$945.7</h4>
                                <label>payment</label>
                            </div>
                            <div class="col-lg-4 mb-2">
                                <label>This Week</label>
                                <h3 class="text-warning">37</h3>
                                <label>hours</label>
                            </div>
                        </div>
                    </div>

                    <!-- TODO: add clickable link to Edit Profile page -->
                    <div class="card-footer text-muted text-center" >
                        <a href="/availabilities"><i class="fa fa-edit"></i>
                        <small class="font-weight-bold">Click here to Edit Availability</small></a>
                    </div>
                </div>
            </div>

            <!-- Right panel -->
            <div class="col-lg-8 mb-1">
                <div class="card">
                    <div class="card-header">
                        <h5><i class="fa fa-check"></i> Schedule </h5>
                    </div>
                    <div class="card-body">
                        <!-- form for Select store and Change time -->
                        <form:form>
                            <div class="form-row">
                                <div class="col-sm-6">
                                    <select class="form-control" id="sel1">
                                        <option selected>Rolld: Monash </option>
                                        <option>Rolld: 357 South Gate</option>
                                        <option>MCD: 234 Wellington Road</option>
                                    </select>
                                </div>
                                <div class="col-sm-6 text-center mt-2">
                                    <label class="font-weight-bold "><i class="fa fa-chevron-left fa-color-primary"></i>
                                        &nbsp; 25 June 2018 - 1 July 2018 &nbsp;
                                        <i class="fa fa-chevron-right fa-color-primary"></i>
                                    </label>
                                </div>
                            </div>
                        </form:form>


                        <div class="row" id="tableMySchedule">
                            <!--table mock, todo: Make into Javascript -->
                            <table class="table table-hover ">
                                <thead class="text-center table-warning">
                                <tr>
                                    <th>Day</th>
                                    <th>Roles</th>
                                    <th>Time</th>
                                    <th>Details</th>
                                </tr>
                                </thead>
                                <tbody class="text-center">
                                <tr>
                                    <td>Wednesday</td>
                                    <td>Summary</td>
                                    <td>09:00 - 11:30</td>
                                    <td><button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modalSchedule">More</button></td>
                                </tr>
                                <tr class="table-active">
                                    <td>Thursday</td>
                                    <td>POS 1</td>
                                    <td>09:00 - 11:30</td>
                                    <td><button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modalSchedule">More</button></td>
                                </tr>
                                <tr>
                                    <td>Friday</td>
                                    <td>Pho + Fryer</td>
                                    <td>08:00 - 12:30</td>
                                    <td><button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modalSchedule">More</button></td>
                                </tr>
                                <!-- Mock -->
                                <tr>
                                    <td>Saturday</td>
                                    <td>Bao + Bun</td>
                                    <td>09:00 - 11:30</td>
                                    <td><button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modalSchedule">More</button></td>
                                </tr>
                                <tr>
                                    <td>Sunday</td>
                                    <td>Bahn mi + Goi</td>
                                    <td>05:00 - 11:30</td>
                                    <td><button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modalSchedule">More</button></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Full Roster -->
        <div class="row mt-3">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h5><i class="fa fa-calendar"></i> Full Roster </h5>
                    </div>
                    <div class="card-body">
                        <!-- Use timetable -->
                        <div class="text-center mt-2">
                            <h3>Rolld: Monash</h3>
                            <p class="text-muted">25 June 2018 - 1 July 2018</p>
                        </div>
                        <div class="timetable mt-2"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- TODO: Modal for the schedule page -->
    <!-- Pop-up More Modal-->
    <div class="modal fade" id="modalSchedule" tabindex="-1" role="dialog" aria-labelledby="modalScheduleLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="modalScheduleLabel">More Details</h3>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <dl class="row text-center">
                        <dt class="col-sm-4 ">Store</dt>
                        <dd class="col-sm-8">Rolld:Monash</dd>

                        <dt class="col-sm-4">Day</dt>
                        <dd class="col-sm-8">Wednesday</dd>

                        <dt class="col-sm-4">Roles</dt>
                        <dd class="col-sm-8">Summary</dd>

                        <dt class="col-sm-4">Time</dt>
                        <dd class="col-sm-8">09:00 - 11:30</dd>

                        <dt class="col-sm-4">Break</dt>
                        <dd class="col-sm-8">10:00 - 10:30</dd>

                        <dt class="col-sm-4">Working Hours</dt>
                        <dd class="col-sm-8">5 hours</dd>

                        <dt class="col-sm-4 text-warning">Extra Notes</dt>
                        <dd class="col-sm-8">Make Catering order for John</dd>

                    </dl>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer -->
    <footer class="sticky-footer">
        <div class="container">
            <div class="text-center">
                <small>Copyright © by Zest Developer, 2018</small>
            </div>
        </div>
    </footer>


    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-warning" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>
</div><!--
<security:authorize access="hasAnyAuthority('ROLE_MANAGER', 'ROLE_EMPLOYEE')">
    <div>User has logged in  </div>
</security:authorize>

<security:authorize access="hasAuthority('ROLE_MANAGER')">
    <div>This user is a coordinator</div>
</security:authorize>
-->
</body>
<!-- Bootstrap core JavaScript-->

<script src="${pageContext.request.contextPath}/resources/vendor/jquery/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="${pageContext.request.contextPath}/resources/vendor/jquery-easing/jquery.easing.min.js"></script>
<!-- Custom scripts for all pages-->
<script src="${pageContext.request.contextPath}/resources/js/index.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/timetable.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/myschedule.js"></script>

</html>

