<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Zest Developer">

    <title>RosterApp</title>
    <!-- Bootstrap core CSS-->
    <link href="${pageContext.request.contextPath}/resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="${pageContext.request.contextPath}/resources/vendor/font-awesome/css/font-awesome.css" rel="stylesheet"
          type="text/css">
    <!-- Custom styles for this template-->
    <link href="${pageContext.request.contextPath}/resources/css/index.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/interiorpage.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/timetablejs.css" rel="stylesheet">

    <!-- Logo -->
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="mobile-web-app-capable" content="yes"/>
    <link rel="shortcut icon" sizes="196x196" href="${pageContext.request.contextPath}/resources/img/logo.png"/>
    <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/resources/img/logo.png"/>
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a href="/home"><img src="${pageContext.request.contextPath}/resources/img/roster_i.png" width="150px" class="navbar-brand"></a>
</nav>

<!-- Content -->
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Profile editor -->
        <div class="shadow card card-register mx-auto mt-2">
            <div class="card-header">
                <h5><i class="fa fa-edit"></i> Edit Profile </h5>
            </div>
            <div class="card-body">
                <form:form action="${pageContext.request.contextPath}/authentication/login/process" method="POST"
                           class="form-horizontal">
                    <!-- Sign Up Form -->
                    <h4 align="center">Personal Information </h4>

                    <div class="text-center">
                        <div class="form-group">
                            <div class="pic-container pic-medium pic-circle">
                                <img src="${pageContext.request.contextPath}/resources/img/person.png"
                                     alt="default profile pic" class="pic" id="profilePic">
                                <input type="file" name="filename" accept="image/gif, image/jpeg, image/png" id="uploadPic">
                                <div id="editPic" class="pic-overlay" onclick="uploadPic()">
                                    Change Profile Picture
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputFirstName">First Name</label>
                            <input type="first_name" class="form-control" id="inputFirstName" placeholder="John">
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputLastName">Last Name</label>
                            <input type="last_name" class="form-control" id="inputLastName" placeholder="Doe">
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                        </div>
                    </div>

                    <!-- DOB input -->
                    <div class="form-group">
                        <label>Date of Birth</label>
                        <input class="form-control" id="dateofBirth" placeholder="Date of Birth" type="date">
                    </div>


                    <!-- Address input -->
                    <div class="form-group">
                        <label for="inputAddress">Address</label>
                        <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputCity">City</label>
                            <input type="text" class="form-control" id="inputCity" placeholder="Melbourne">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputState">State</label>
                            <input type="text" class="form-control" id="inputState" placeholder="VIC">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputZip">Zip</label>
                            <input type="text" class="form-control" id="inputZip" placeholder="3000">
                        </div>
                    </div>


                    <hr>
                    <h4 align="center"> Log In Credentials </h4>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="control-label" for="Email">Change Email:</label>
                        <div class="controls">
                            <input id="Email" name="Email" class="form-control" type="text"
                                   placeholder="email@email.com" class="input-large" required="">
                        </div>
                    </div>

                    <!-- Password input-->
                    <div class="form-group">
                        <label class="control-label" for="password">Change Password:</label>
                        <div class="controls">
                            <input id="password" name="password" class="form-control" type="password"
                                   placeholder="********" class="input-large" required="">
                            <small id="passwordHelpBlock" class="form-text text-muted">
                                Your password must be 8-20 characters long, contain letters and numbers, and must not
                                contain spaces, special characters, or emoji.
                            </small>

                        </div>
                    </div>

                    <!-- Button Cancel -->
                    <div class="form-group" align="center">
                        <label class="control-label" for="confirmChange"></label>
                        <div class="controls">
                            <button id="confirmChange" type="submit" class="btn btn-primary btn-block">Save Changes
                            </button>
                        </div>
                    </div>
                </form:form>
                <button id="cancelSignup" onclick="window.history.back()" class="btn btn-warning btn-block">Cancel
                </button>
            </div>
        </div>

    </div>


    <!-- Footer -->
    <footer class="sticky-footer">
        <div class="container">
            <div class="text-center">
                <small>Copyright © by Zest Developer, 2018</small>
            </div>
        </div>
    </footer>


    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-warning" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <security:authorize access="hasAnyAuthority('ROLE_MANAGER', 'ROLE_EMPLOYEE')">
        <!--<div>User has logged in  </div>

    </security:authorize>

    <security:authorize access="hasAuthority('ROLE_MANAGER')">
    <div>This user is a coordinator</div> -->
    </security:authorize>
</div>


</body>
<!-- Bootstrap core JavaScript-->

<script src="${pageContext.request.contextPath}/resources/vendor/jquery/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="${pageContext.request.contextPath}/resources/vendor/jquery-easing/jquery.easing.min.js"></script>
<!-- Custom scripts for all pages-->
<script src="${pageContext.request.contextPath}/resources/js/index.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/timetable.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/myschedule.js"></script>

<%-- Edit profile javascript --%>
<script src="${pageContext.request.contextPath}/resources/js/editProfile.js"></script>

</html>

