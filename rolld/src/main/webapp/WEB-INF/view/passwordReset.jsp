<%--
  Created by IntelliJ IDEA.
  User: cooper
  Date: 19/06/18
  Time: 12:36 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<body>
<h1>reset password page</h1>

<form:form action="${pageContext.request.contextPath}/authentication/save-new-password" method="POST">
    <span>reset password</span>
    <input type="text" name="new-password" placeholder="password">
    <input type="text" name="new-password-confirm" placeholder="confirm password">

    <input type="submit">

</form:form>

</body>
<!-- Bootstrap core JavaScript-->
<script src="${pageContext.request.contextPath}/resources/vendor/jquery/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="${pageContext.request.contextPath}/resources/vendor/jquery-easing/jquery.easing.min.js"></script>

</html>