<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Zest Developer">

    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>

    <title>RosterApp</title>
    <!-- Bootstrap core CSS-->
    <link href="${pageContext.request.contextPath}/resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="${pageContext.request.contextPath}/resources/vendor/font-awesome/css/font-awesome.css" rel="stylesheet"
          type="text/css">
    <!-- Custom styles for this template-->
    <link href="${pageContext.request.contextPath}/resources/css/index.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/interiorpage.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/timetablejs.css" rel="stylesheet">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css"/>
    <!-- Logo -->
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="mobile-web-app-capable" content="yes"/>
    <link rel="shortcut icon" sizes="196x196" href="${pageContext.request.contextPath}/resources/img/logo.png"/>
    <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/resources/img/logo.png"/>
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <img src="${pageContext.request.contextPath}/resources/img/roster_i.png" width="150px" class="navbar-brand"
         href="*">
</nav>

<!-- Content -->
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Add Availabilities menu -->
        <div class="card mb-2">
            <div class="card-header">
                <h5><i class="fa fa-calendar-plus-o"></i> Add Availabilities </h5>
            </div>
            <div class="card-body">
                <form id="clearField">
                    <div class="form-group row">
                        <label for="idealHours" class="col-4 col-form-label">Ideal Hours to Work</label>
                        <div class="col-8">
                            <input class="form-control" type="text" id="idealHours" placeholder="">
                        </div>
                    </div>
                </form>

                <hr>
                <!-- Buttons -->

                <div class="text-center">
                    <button class="btn btn-primary text-center" data-toggle="modal" data-target="#modalAddAvailabilities"><i class="fa fa-plus"></i> Add More Availabilities</button>
                </div>
                <hr>
                <div class="text-center">
                    <h5> Availabilities: </h5>
                    <div id="additionalHoursData">
                        <small id="emptyExtra" class="text-muted">Nothing to show at the moment. <br> If you wish to add more availability,
                            please click "Add More Availabilities" button below.</small>
                        <!-- Add more availabilities below
                        While adding, at the right side gives (X) button
                        so it can be deleted if not needed anymore
                        -->
                    </div>
                </div>
                <hr>
                <div class="text-center">
                    <button class="btn btn-danger text-center" ><i class="fa fa-refresh"></i> Delete All</button>
                    <button class="btn btn-warning text-center"><i class="fa fa-save"></i> Save Changes</button>
                </div>

                </div>
            </div>
        </div>
    </div>
    <!-- TODO: Modal for the schedule page -->
    <!-- Pop-up More Modal-->
    <div class="modal fade" id="modalAddAvailabilities" tabindex="-1" role="dialog" aria-labelledby="modalScheduleLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="modalAvailabilitiesLabel">Add More Availabilities </h3>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form name="addExtra">
                <div class="modal-body">
                        <div class="form-group row">
                            <label for="newDate" class="col-sm-4 col-form-label">Select Day</label>
                            <div class="col-sm-8">
                                <select class="form-control" id="newDate">
                                    <option value="Monday">Monday</option>
                                    <option value="Tuesday">Tuesday</option>
                                    <option value="Wednesday">Wednesday</option>
                                    <option value="Thursday">Thursday</option>
                                    <option value="Friday">Friday</option>
                                    <option value="Saturday">Saturday</option>
                                    <option value="Sunday">Sunday</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="newDate" class="col-sm-4 col-form-label">Add Time</label>

                            <div class="col-4">
                                <div class="form-group">
                                    <div class="input-group date" id="newStart" data-target-input="nearest">
                                        <input id="newStartInput" type="text" class="form-control datetimepicker-input" data-target="#newStart"
                                               placeholder="Start"/>
                                        <div class="input-group-append" data-target="#newStart"
                                             data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="form-group">

                                    <div class="input-group date" id="newEnd" data-target-input="nearest">
                                        <input id="newEndInput" type="text" class="form-control datetimepicker-input" data-target="#newEnd"
                                               placeholder="End"/>
                                        <div class="input-group-append" data-target="#newEnd" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>

                    <button id="addAdditionalAv" class="btn btn-warning" type="button" onclick="validateForm()" data-dismiss="modal">Add</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Footer -->
    <footer class="sticky-footer">
        <div class="container">
            <div class="text-center">
                <small>Copyright © by Zest Developer, 2018</small>
            </div>
        </div>
    </footer>


    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-warning" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>



</div>


</body>
<!-- Bootstrap core JavaScript-->

<script src="${pageContext.request.contextPath}/resources/vendor/jquery/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- angular js -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.2/angular.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="${pageContext.request.contextPath}/resources/vendor/jquery-easing/jquery.easing.min.js"></script>
<!-- Custom scripts for all pages-->
<script src="${pageContext.request.contextPath}/resources/js/index.js"></script>

<%-- Availabilities javascript --%>
<script src="${pageContext.request.contextPath}/resources/js/availabilities.js"></script>


<script>
    var csrf_token = $("meta[name='_csrf']").attr("content");
    var csrf_header = $("meta[name='_csrf_header']").attr("content");

    function loadAvaliabilities() {
        $.ajax({
             type: "GET",
             contentType: "application/json",
             url: "${pageContext.request.contextPath}/home/availabilities/user-availability",
             dataType: 'json',
             timeOut: 10000,
             beforeSend: function(request) {  // append csrf token to request header
                 request.setRequestHeader(csrf_header, csrf_token);
             },

             success: function (data) {
                 console.log("ajax succeeded");
                 console.log(data);
             },
             error: function (e) {
                 console.log("ajax failed");
                 console.log(e);
             },
             done: function (e) {
                 console.log("ajax finished. availability entries created");
             }
         });

    }


    $("#addAdditionalAv").click(function createAvailabilityEntries() {
        console.log("ajax request sent");
        var requestJSON = {newDate: $("#newDate").val(), availabilityStartTime: $("#newStartInput").val(), availabilityEndTime: $("#newEndInput").val()};  // data sent to the server
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "${pageContext.request.contextPath}/home/availabilities/availability-save",
            data: JSON.stringify(requestJSON),
            dataType: 'json',
            timeOut: 10000,
            beforeSend: function(request) {  // append csrf token to request header
                request.setRequestHeader(csrf_header, csrf_token);
            },

            success: function (data) {
                console.log("ajax succeeded");
            },
            error: function (e) {
                console.log("ajax failed");
                console.log(e);
            },
            done: function (e) {
                console.log("ajax finished. availability entries created");
            }
        });
    });



</script>
</html>


