<%--
  Created by IntelliJ IDEA.
  User: cooper
  Date: 5/07/18
  Time: 8:48 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Forget Password</title>
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
</head>
<body>
    <span>Email</span>
    <input id="email" type="email" name="email" placeholder="Email">
    <button onclick="conformEmailForPWReset()">Send </button>
    <div id="message">-</div>

    <!-- Bootstrap core JavaScript-->
    <script src="${pageContext.request.contextPath}/resources/vendor/jquery/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="${pageContext.request.contextPath}/resources/vendor/jquery-easing/jquery.easing.min.js"></script>

    <script>
    /**
     * Confirm the email where the reset token is sent
     */
    function conformEmailForPWReset() {  // query source harbor
        var csrf_token = $("meta[name='_csrf']").attr("content");
        var csrf_header = $("meta[name='_csrf_header']").attr("content");
        console.log(csrf_token);
        console.log(csrf_header);
        console.log("ajax request sent");
        var requestJSON = {email: $("#email").val()};  // data sent to the server
        console.log(requestJSON);
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "${pageContext.request.contextPath}/authentication/send-password-token",
            data: JSON.stringify(requestJSON),
            dataType: 'json',
            timeOut: 10000,
            beforeSend: function(request) {  // append csrf token to request hearder
                request.setRequestHeader(csrf_header, csrf_token);
            },

            success: function (data) {
                console.log("ajax succeeded");
                $("#message").text("okay");
            },
            error: function (e) {
                console.log("ajax failed");
                $("#message").text("failed");
                console.log(e);
            },
            done: function (e) {
                console.log("ajax finished");
                console.log(e);
            }

        });

    }

</script>

</body>
</html>
