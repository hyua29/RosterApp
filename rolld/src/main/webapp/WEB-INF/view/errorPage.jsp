<%--
  Created by IntelliJ IDEA.
  User: riord
  Date: 27/06/2018
  Time: 3:11 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Zest Developer">
    <title>RosterApp</title>
    <!-- Bootstrap core CSS-->
    <link href="${pageContext.request.contextPath}/resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="${pageContext.request.contextPath}/resources/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template-->
    <link href="${pageContext.request.contextPath}/resources/css/index.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/login.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/custom.css" rel="stylesheet">
    <!-- Logo -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="mobile-web-app-capable" content="yes" />
    <link rel="shortcut icon" sizes="196x196" href="${pageContext.request.contextPath}/resources/img/logo.png" />
    <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/resources/img/logo.png" />
</head>

<body background="${pageContext.request.contextPath}/resources/img/bg1.png" class="login-bg">
<div class="container">
    <div class="shadow card card-login mx-auto mt-5">
        <div style="text-align: center;" class="card-header">
            <h1 class="text-danger"> Error 404!</h1>
        </div>
        <div class="card-body">
            <div class="text-center">
                <h2>Oh No! Why are you here?</h2>
                <p> You are not supposed to be in here..
                    <br>There is nothing in this page..
                    <br>I also look for something but, meh.. nothing.
                    <br>Please blame developers for this :)
                    <br><br>
                    So, please.. take this and leave me alone..<br>
                    (*you have received "nothing" from old wanderer)</p>
                <!-- return button -->
                <button type="submit" class="btn btn-warning btn-block mt-2">Back To Previous Page</button>

            </div>
        </div>
    </div>
</div>
<!-- Bootstrap core JavaScript-->
<script src="${pageContext.request.contextPath}/resources/vendor/jquery/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="${pageContext.request.contextPath}/resources/vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>