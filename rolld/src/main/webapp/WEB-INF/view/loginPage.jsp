<%--
  Created by IntelliJ IDEA.
  User: riord
  Date: 27/06/2018
  Time: 3:11 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Zest Developer">
    <title>RosterApp</title>
    <!-- Bootstrap core CSS-->
    <link href="${pageContext.request.contextPath}/resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="${pageContext.request.contextPath}/resources/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template-->
    <link href="${pageContext.request.contextPath}/resources/css/index.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/login.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/custom.css" rel="stylesheet">
    <!-- Logo -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="mobile-web-app-capable" content="yes" />
    <link rel="shortcut icon" sizes="196x196" href="${pageContext.request.contextPath}/resources/img/logo.png" />
    <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/resources/img/logo.png" />
</head>

<body background="${pageContext.request.contextPath}/resources/img/bg1.png" class="login-bg">
<div class="container">
    <div class="shadow card card-login mx-auto mt-5">
        <div style="text-align: center;" class="card-header">
            <img src="${pageContext.request.contextPath}/resources/img/roster.png" width="50%">
        </div>
        <div class="card-body">
            <form:form action="${pageContext.request.contextPath}/authentication/login/process" method="POST"
                       class="form-horizontal">
                <!-- Place for messages: error, alert etc ... -->
                <div class="form-group">
                    <div class="col-xs-15">
                        <div>
                            <!-- check whether there is any error -->  <!-- If there is any error, the spring security will send the user back to the login page automatically with
                                'error' as the request parameter -->
                            <c:if test="${param.error!=null}">
                                <div class="alert alert-danger col-xs-offset-1 col-xs-10">
                                    Invalid username and password.
                                </div>
                            </c:if>

                            <c:if test="${param.logout != null}">
                                <div class="alert alert-success col-xs-offset-1 col-xs-10">
                                    You have been logged out.
                                </div>
                            </c:if>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    <input type="text" name="username" placeholder="Username" class="form-control">
                </div>
                <div class="form-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <input type="password" name="password" placeholder="Password" class="form-control">
                </div>
                <button type="submit" class="btn btn-warning btn-block">Log in</button>
            </form:form>

            <div class="text-center">

                <!-- Forget Password link -->
                <a class="d-block small mt-2" href="${pageContext.request.contextPath}/authentication/forget-password">Forgot Password?</a>

                <!-- TODO: remove this link -->
                <a class="d-block small mt-2" href="${pageContext.request.contextPath}/authentication/sign-up">Temp Sign Up</a>

            </div>
        </div>
        <!-- Footer -->
        <div class="card-footer text-center">
            <small>Copyright © by Zest Developer, 2018</small>
        </div>
    </div>
</div>
<!-- Bootstrap core JavaScript-->
<script src="${pageContext.request.contextPath}/resources/vendor/jquery/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="${pageContext.request.contextPath}/resources/vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>