<%--
  Created by IntelliJ IDEA.
  User: riord
  Date: 27/06/2018
  Time: 3:11 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="input" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Zest Developer">
    <title>RosterApp</title>
    <!-- Bootstrap core CSS-->
    <link href="${pageContext.request.contextPath}/resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="${pageContext.request.contextPath}/resources/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template-->
    <link href="${pageContext.request.contextPath}/resources/css/index.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/login.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/custom.css" rel="stylesheet">
    <!-- Logo -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="mobile-web-app-capable" content="yes" />
    <link rel="shortcut icon" sizes="196x196" href="${pageContext.request.contextPath}/resources/img/logo.png" />
    <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/resources/img/logo.png" />
</head>

<body background="${pageContext.request.contextPath}/resources/img/bg1.png" class="login-bg">
<div class="container">
    <div class="shadow card card-register mx-auto mt-5">
        <div style="text-align: center;" class="card-header">
            <img src="${pageContext.request.contextPath}/resources/img/roster.png" width="200rem">
        </div>
        <div class="card-body">
            <form:form action="${pageContext.request.contextPath}/authentication/sign-up-confirm" modelAttribute='user-details' method="POST"
                       class="form-horizontal">
                <!-- Sign Up Form -->
                <h4 align="center">Personal Information </h4>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputFirstName">First Name</label>
                        <form:input type="text" class="form-control" id="inputFirstName" path="fName" placeholder="First Name" required="required" />
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputLastName">Last Name</label>
                        <form:input type="last_name" class="form-control" id="inputLastName" path="lName" placeholder="Last Name" />
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </div>
                </div>

                <!-- DOB input -->
                <div class="form-group">
                    <label>Date of Birth</label>
                    <form:input class="form-control" id="dateofBirth" path="doB" placeholder="Date of Birth" type="date" required="required"/>
                </div>


                <%--<!-- Address input -->--%>  <!-- TODO: consider to remove, address not required -->
                <%--<div class="form-group">--%>
                    <%--<label for="inputAddress">Address</label>--%>
                    <%--<input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">--%>
                <%--</div>--%>

                <%--<div class="form-row">--%>
                    <%--<div class="form-group col-md-6">--%>
                        <%--<label for="inputCity">City</label>--%>
                        <%--<input type="text" class="form-control" id="inputCity" placeholder="Melbourne">--%>
                    <%--</div>--%>
                    <%--<div class="form-group col-md-4">--%>
                        <%--<label for="inputState">State</label>--%>
                        <%--<input type="text" class="form-control" id="inputState" placeholder="VIC">--%>
                    <%--</div>--%>
                    <%--<div class="form-group col-md-2">--%>
                        <%--<label for="inputZip">Zip</label>--%>
                        <%--<input type="text" class="form-control" id="inputZip" placeholder="3000">--%>
                    <%--</div>--%>
                <%--</div>--%>

                <hr>
                <h4 align="center"> Log In Credentials </h4>

                <%--<!-- Text input-->--%>  <!-- TODO: consider to remove, email is attached with invitation token -->
                <%--<div class="form-group">--%>
                    <%--<label class="control-label" for="Email">Email:</label>--%>
                    <%--<div class="controls">--%>
                        <%--<input id="Email" name="Email" class="form-control" type="text" placeholder="email@email.com" class="input-large" required="">--%>
                    <%--</div>--%>
                <%--</div>--%>

                <!-- Password input-->
                <div class="form-group">
                    <label class="control-label" for="password">Password:</label>
                    <div class="controls">
                        <form:input id="password" name="password" class="form-control input-large" type="password" path="password" placeholder="********" required="required"/>
                        <small id="passwordHelpBlock" class="form-text text-muted">
                            Your password must be 8-20 characters long, contain letters and numbers, and must not contain spaces, special characters, or emoji.
                        </small>

                    </div>
                </div>

                <!-- Reenter Password input-->
                <div class="form-group">
                    <label class="control-label" for="reenterpassword">Re-Enter Password:</label>
                    <div class="controls">
                        <form:input id="reenterpassword" class="form-control input-large" name="reenterpassword" type="password" path="passwordConfirm" placeholder="********" required="required"/>
                    </div>
                </div>

                <!-- Terms and Condition -->
                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                        <label class="form-check-label" for="invalidCheck">
                            Agree to terms and conditions
                        </label>
                        <div class="invalid-feedback">
                            You must agree before submitting.
                        </div>
                    </div>
                </div>

                <!-- Button Cancel -->
                <div class="form-group" align="center">
                    <label class="control-label" for="confirmSignup"></label>
                    <div class="controls">
                        <button id="confirmsignup" type="submit" class="btn btn-primary btn-block">Sign up</button>
                    </div>
                </div>
            </form:form>
            <button id="cancelSignup" href="" class="btn btn-warning btn-block">Cancel</button>
        </div>
    </div>
</div>
<!-- Bootstrap core JavaScript-->
<script src="${pageContext.request.contextPath}/resources/vendor/jquery/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="${pageContext.request.contextPath}/resources/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/signup.js"></script>
</body>

</html>