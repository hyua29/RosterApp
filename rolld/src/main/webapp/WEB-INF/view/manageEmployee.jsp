<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Zest Developer">
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>

    <title>RosterApp</title>
    <!-- Bootstrap core CSS-->
    <link href="${pageContext.request.contextPath}/resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="${pageContext.request.contextPath}/resources/vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template-->
    <link href="${pageContext.request.contextPath}/resources/css/index.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/interiorpage.css" rel="stylesheet">

    <!-- Logo -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="mobile-web-app-capable" content="yes" />
    <link rel="shortcut icon" sizes="196x196" href="${pageContext.request.contextPath}/resources/img/logo.png" />
    <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/resources/img/logo.png" />
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a href="/home"><img src="${pageContext.request.contextPath}/resources/img/roster_i.png" width="150px" class="navbar-brand" href="*"></a>
</nav>

<!-- Content -->
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <!-- Change the Store -->
            <div class="col-lg-12">
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label"><strong>Change Store: </strong></label>
                    <div class="col-sm-6 mb-2">
                        <select id="storeName" class="form-control form-control-sm">
                            <c:forEach var="storeName" items="${storeNames}">
                                <option value="${storeName}" selected> ${storeName}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="col-sm-3 text-right">
                        <button onclick="getEmployeeDetails($('#storeName').val())" class="btn btn-sm btn-warning">
                            Change
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <!-- Add User to this Store -->
            <div class="col-lg-4 mb-2 mt-2">
                <div class="card">
                    <div class="card-header">
                        <h4><i class="fa fa-user-plus"></i> Add Employee </h4>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="emailEmployee">Email address</label>
                            <input type="email" class="form-control" id="emailEmployee" aria-describedby="emailHelp" placeholder="Enter email">
                            <small id="emailHelp" class="form-text text-muted">Please enter employee's email address to invite employee into this store.</small>
                        </div>

                        <div class="form-group">
                            <label>Select employee's type:  </label>
                            <select id="employType" class="form-control form-control-sm">
                                <option selected disabled>Choose one...</option>
                                <option value="C"> Casual </option>
                                <option value="PT">Part Time</option>
                                <option value="P"> Full Time</option>
                            </select>


                        </div>
                        <button type="submit" onclick="sendEmailForInvitation()" class="btn btn-primary btn-block">Add</button>


                    </div>
                    <div class="card-footer">
                        <small class="text-muted">
                            To <strong> edit / remove </strong> employee, please refer to the Manage Employees section.
                        </small>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 mb-2 mt-2">
                <div class="card">
                    <div class="card-header">
                        <h4><i class="fa fa-users"></i> Manage Employees </h4>
                    </div>
                    <div class="card-body ">
                        <div class="row text-center">
                            <div class="col-lg-3 ">
                                <img alt="Profile Picture" width="100px" class="img-circle" src="${pageContext.request.contextPath}/resources/img/store.png">
                            </div>
                            <div class="col-lg-9">
                                <h3>Rolld: Monash</h3>
                                <label class="text-muted">Number of Employees: 30</label>
                            </div>
                        </div>
                        <hr>
                        <div >
                            <h4>List of Employees: </h4>
                            <div class="list-group">
                                <button type="button" class="list-group-item list-group-item-action" data-toggle="modal" data-target="#modalEditEmployee">John Doe</button>
                                <button type="button" class="list-group-item list-group-item-action" data-toggle="modal" data-target="#modalEditEmployee">Jane Dae</button>
                                <button type="button" class="list-group-item list-group-item-action" data-toggle="modal" data-target="#modalEditEmployee">Cone Doe</button>
                                <button type="button" class="list-group-item list-group-item-action" data-toggle="modal" data-target="#modalEditEmployee">Jasmine Doe</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer -->
    <footer class="sticky-footer">
        <div class="container">
            <div class="text-center">
                <small>Copyright © by Zest Developer, 2018</small>
            </div>
        </div>
    </footer>


    <!-- Pop-up More Modal-->
    <div class="modal fade" id="modalEditEmployee" tabindex="-1" role="dialog" aria-labelledby="modalScheduleLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="modalScheduleLabel">More Details</h3>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group text-center">
                        <h3 class="text-primary ">John Doe</h3>
                        <div class="form-group row">
                            <label class="col-sm-5 col-form-label mt-3"><strong>Date of Birth</strong></label><br>
                            <div class="col-sm-7 col-form-label mt-3">
                                dd/mm/yyyy
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-5 col-form-label"><strong>Salary</strong></label><br>
                            <div class="col-sm-7 col-form-label">
                                $20
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-5 col-form-label"><strong>Ideal Hours</strong></label><br>
                            <div class="col-sm-7 col-form-label">
                                20
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-5 col-form-label"><strong>Availabilities</strong></label><br>
                            <div class="col-sm-12 table-responsive mt-3">
                                <table class="table table-sm text-center">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Day</th>
                                        <th scope="col">Start</th>
                                        <th scope="col">End</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">Monday</th>
                                        <td>3.00 p.m.</td>
                                        <td>5.00 p.m.</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-warning" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>
    <div>
        <button onclick="getEmployeeDetails()">click me</button>
    </div>
    <!--
    <security:authorize access="hasAnyAuthority('ROLE_MANAGER', 'ROLE_EMPLOYEE')">
        <div>User has logged in </div>
    </security:authorize>

    <security:authorize access="hasAuthority('ROLE_MANAGER')">
        <div>This user is a coordinator</div>
    </security:authorize>
    -->
</div>

</body>
<script src="${pageContext.request.contextPath}/resources/vendor/jquery/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="${pageContext.request.contextPath}/resources/vendor/jquery-easing/jquery.easing.min.js"></script>
<!-- Custom scripts for all pages-->
<script src="${pageContext.request.contextPath}/resources/js/index.js"></script>
<script>
    var currentStoreUserDetails;  // this records all the users' details of current store
    var csrf_token = $("meta[name='_csrf']").attr("content");
    var csrf_header = $("meta[name='_csrf_header']").attr("content");

    (function () {
        var csrf_token = $("meta[name='_csrf']").attr("content");
        var csrf_header = $("meta[name='_csrf_header']").attr("content");
        console.log(csrf_token);
        console.log(csrf_header);

        getEmployeeDetails() // load all employees after the page is fully loaded
    })();


    /**
     * Get All employees' details from the selected store through ajax
     *
     *
     */
    function getEmployeeDetails() {

        $.ajax({
            type: "GET",
            url: "${pageContext.request.contextPath}/store-manager/current-store-employee-details?"
            + "store-name=" + $("#storeName").val(),
            dataType: 'json',
            timeOut: 10000,
            beforeSend: function(request) {  // append csrf token to request header
                request.setRequestHeader(csrf_header, csrf_token);
            },
            success: function (data) {  // TODO: put the data to the page
                console.log("ajax succeeded");
                currentStoreUserDetails = data.resultMap;
                console.log(data.resultMap);
            },
            error: function (e) {
                console.log("ajax failed");
                console.log(e);
            },
            done: function (e) {
                console.log("ajax finished");
                console.log(e);
            }

        });

    }


    /**
     * create a register token and send it to the user via email
     */
    function sendEmailForInvitation() {
        var csrf_token = $("meta[name='_csrf']").attr("content");
        var csrf_header = $("meta[name='_csrf_header']").attr("content");
        console.log(csrf_token);
        console.log(csrf_header);
        console.log("ajax request sent");
        var requestJSON = {email: $("#emailEmployee").val(), employType: $("#employType").val(), storeName: $("#storeName").val() };  // data sent to the server
        console.log(requestJSON);

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "${pageContext.request.contextPath}/authentication/new-user-invite",
            data: JSON.stringify(requestJSON),
            dataType: 'json',
            timeOut: 10000,
            beforeSend: function(request) {  // append csrf token to request header
                request.setRequestHeader(csrf_header, csrf_token);
            },

            success: function (data) {
                console.log("ajax succeeded");
            },
            error: function (e) {
                console.log("ajax failed");
                console.log(e);
            },
            done: function (e) {
                console.log("ajax finished");
                console.log(e);
            }

        });

    }

</script>

</html>
