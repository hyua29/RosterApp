
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Zest Developer">
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>

    <title>RosterApp</title>
    <!-- Bootstrap core CSS-->
    <link href="${pageContext.request.contextPath}/resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="${pageContext.request.contextPath}/resources/vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template-->
    <link href="${pageContext.request.contextPath}/resources/css/index.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/interiorpage.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/timetablejs.css" rel="stylesheet">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css"/>
    <link href="${pageContext.request.contextPath}/resources/css/snackbar.css" rel="stylesheet">
    <!-- Logo -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="mobile-web-app-capable" content="yes" />
    <link rel="shortcut icon" sizes="196x196" href="${pageContext.request.contextPath}/resources/img/logo.png" />
    <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/resources/img/logo.png" />
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a href="/home"><img src="${pageContext.request.contextPath}/resources/img/roster_i.png" width="150px" class="navbar-brand" href="*"></a>
</nav>

<!-- Content -->
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <!-- Roster panel -->
            <div class="col-lg-12 mb-1">
                <div class="card">
                    <div class="card-header">
                        <h4><i class="fa fa-calendar-check-o"></i> Rostering </h4>
                    </div>
                    <div class="card-body">
                        <!-- form for Select store and Change time -->
                        <form:form>
                            <div class="form-row">
                                <div class="col-sm-6 mb-2">
                                    <label>Select Store: </label>
                                    <select class="form-control" id="sel1">
                                        <c:forEach var="storeName" items="${storeNames}">
                                            <option value="${storeName}" selected> ${storeName}</option>
                                        </c:forEach>
                                    </select>
                                </div>

                            </div>
                        </form:form>
                        <hr>

                        <!-- Tools -->
                        <div class="row mb-lg-2">
                            <div class="col-5">
                                <!-- Day Form -->
                                <div class="form-group">
                                    <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker4"/>
                                        <div class="input-group-append text-center" data-target="#datetimepicker4" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 mb-2">
                                <button type="button" class="btn btn-warning btn-sm mb-2" data-toggle="modal" data-target="#editRoleModal">
                                    <li class="fa fa-edit"></li>
                                    Edit Roles
                                </button>
                                <button type="button" class="btn btn-primary btn-sm mb-2" data-toggle="modal" data-target="#addShiftModal">
                                    <li class="fa fa-plus"></li>
                                    Add Shift
                                </button>

                            </div>

                            <div class="col-lg-2 text-right">
                                <label><strong>Status :</strong> </label>
                                <label id="rosterStatus" class="text-success"> Submitted </label>
                            </div>
                        </div>

                        <div class="timetable mt-0"></div>
                        <label class="small text-muted mb-2 mt-2"><strong>Hint:</strong> To edit a shift, please click corresponding shift inside the table.</label>

                        <div class="row mt-2">
                            <div class="col-lg-12 text-center">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-secondary btn-md mt-2 mr-2" onclick="showSnackbar()">
                                    <li class="fa fa-save"></li>
                                    Save Draft
                                </button>
                                <button type="button" class="btn btn-warning btn-md mt-2 " data-toggle="modal" data-target="#exampleModalCenter">
                                    <li class="fa fa-send"></li>
                                    Submit & Send
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal for Remove Shift -->
    <div class="modal fade" id="editRoleModal" tabindex="-1" role="dialog" aria-labelledby="editRoleModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="editRoleModalTitle"> Edit Roles </h3>
                </div>
                <div class="modal-body">
                    <form:form>
                        <div class="form-group row">
                            <label for="addRole" class="col-sm-3 col-form-label"><strong>Add Role</strong></label>
                            <div class="col-sm-7 mb-2">
                                <input type="text" class="form-control form-control-sm" id="addRole"  placeholder="Enter role here...">
                            </div>
                            <div class="col-sm-2 text-right ">
                                <button class="btn btn-sm btn-warning">
                                    <i class="fa fa-plus"></i>
                                    Add
                                </button>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"><strong>Delete Role</strong></label>
                            <div class="col-sm-6 mb-2">
                                <select id="removeRole" class="form-control form-control-sm">
                                    <option selected> Fryer and Cook </option>
                                    <option>...</option>
                                </select>
                            </div>
                            <div class="col-sm-3 text-right">
                                <button class="btn btn-sm btn-danger">
                                    <i class="fa fa-minus"></i>
                                    Delete
                                </button>
                            </div>
                        </div>
                    </form:form>

                    <hr>
                    <small> <strong class="text-warning">Hint: </strong> <strong>Add role</strong> means adding role into the store's role.
                        <strong>Delete role</strong> means deleting role from the store's role.
                    </small>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal for Add Shift-->
    <div class="modal fade" id="addShiftModal" tabindex="-1" role="dialog" aria-labelledby="removeShiftModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="addShiftModalTitle"> Add Shift</h3>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-lg-6">
                            <h5 class="text-primary"> Wednesday</h5>
                            <label class="text-secondary">27 June 2018</label>
                        </div>
                        <div class="col-lg-6 text-right">
                            <label>Rolld: Monash</label>
                            <label class="text-muted"> 25 June 2018 - 1 July 2018</label>
                        </div>
                    </div>
                    <hr>

                    <form:form>
                        <div class="form-group row">
                            <label for="rolesName" class="col-sm-3 col-form-label">Role Name</label>
                            <div class="col-sm-9">
                                <select id="rolesName" class="form-control">
                                    <option selected> Pho + Fryer </option>
                                    <option>...</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Employee</label>
                            <div class="col-sm-9">
                                <select id="inputState" class="form-control">
                                    <option selected> Jane Doe | 15 hours </option>
                                    <option>...</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-row row">
                            <label class="col-sm-3 col-form-label">Working Time</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" placeholder="Start">
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" placeholder="Finish">
                            </div>
                            <label class="small text-muted ml-2"> Has to be in HH:MM format</label>
                        </div>

                        <div class="form-row row">
                            <label class="col-sm-3 col-form-label">Break Time</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" placeholder="Start">
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" placeholder="Finish">
                            </div>
                            <label class="small text-muted ml-2"> Has to be in HH:MM format</label>
                        </div>

                        <div class="form-group">
                            <label for="extraNotes">Extra Notes</label>
                            <textarea class="form-control" id="extraNotes" rows="3"></textarea>
                        </div>
                    </form:form>
                    <label class="sm ml-2 font-italic text-muted">
                        <strong>Hint:</strong> Hours next to employee's name is the assigned hours for respective employee in current week.
                    </label>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-warning">Add Shift</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal for Edit Shift-->
    <div class="modal fade" id="shiftModal" tabindex="-1" role="dialog" aria-labelledby="shiftModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="shiftModalTitle"> Edit Shift</h3>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <h5 class="text-primary"> Wednesday</h5>
                            <label class="text-secondary">27 June 2018</label>
                        </div>
                        <div class="col-lg-6 text-right">
                            <label>Rolld: Monash</label>
                            <label class="text-muted"> 25 June 2018 - 1 July 2018</label>
                        </div>
                    </div>
                    <hr>

                    <form:form>
                        <div class="form-group row">
                            <label for="rolesName" class="col-sm-3 col-form-label">Role Name</label>
                            <div class="col-sm-9">
                                <select id="rolesNameEdit" class="form-control">
                                    <option selected> Pho + Fryer </option>
                                    <option>...</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Employee</label>
                            <div class="col-sm-9">
                                <select id="inputStateEdit" class="form-control">
                                    <option selected> Jane Doe | 15 hours </option>
                                    <option>...</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-row row">
                            <label class="col-sm-3 col-form-label">Working Time</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" placeholder="Start" value="09:00">
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" placeholder="Finish" value="14:30">
                            </div>
                            <label class="small text-muted ml-2"> Has to be in HH:MM format</label>
                        </div>

                        <div class="form-row row">
                            <label class="col-sm-3 col-form-label">Break Time</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" placeholder="Start" value="11:00">
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" placeholder="Finish" value="11:30">
                            </div>
                            <label class="small text-muted ml-2"> Has to be in HH:MM format</label>
                        </div>

                        <div class="form-group">
                            <label for="extraNotes">Extra Notes</label>
                            <textarea class="form-control" id="extraNotesEdit" rows="3">Do dishy when service is not busy.
                            </textarea>
                        </div>
                    </form:form>
                    <label class="sm ml-2 font-italic text-muted">
                        <strong>Hint:</strong> Hours next to employee's name is the assigned hours for respective employee in current week.
                    </label>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger"> Delete</button>
                    <button type="button" class="btn btn-warning">Update</button>

                </div>
            </div>
        </div>
    </div>


    <!-- Modal for Submit -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <i class="fa fa-send fa-2x"></i>
                    <h3 class="modal-title" id="exampleModalLongTitle"> &nbsp; Submit & Send</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            The roster will be submitted. All employees and managers inside the <strong> #Rolld: Monash#</strong>
                            will be notified through e-mail.<br>
                            Do you wish to continue?
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-warning">Continue</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Snackbar for Save draft -->
    <div id="snackbar">
        <label class="text-success">Successful</label> in saving #Rolld: Monash, 25 June 2018 - 1 July 2018# as Draft.
        <br> Older version will be automatically removed.
    </div>

    <!-- Footer -->
    <footer class="sticky-footer">
        <div class="container">
            <div class="text-center">
                <small>Copyright © by Zest Developer, 2018</small>
            </div>
        </div>
    </footer>

    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-warning" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
<!-- Bootstrap core JavaScript-->

<script src="${pageContext.request.contextPath}/resources/vendor/jquery/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="${pageContext.request.contextPath}/resources/vendor/jquery-easing/jquery.easing.min.js"></script>
<!-- Custom scripts for all pages-->
<script src="${pageContext.request.contextPath}/resources/js/index.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/timetable.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/myschedule.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/rostering.js"></script>
<script>

    var csrf_token = $("meta[name='_csrf']").attr("content");
    var csrf_header = $("meta[name='_csrf_header']").attr("content");
    /**
     * Sample response:
     * result : {
     *          roleId: 8,
     *          roleName: "Fryer",
     *          roleComment: "this is fryer",
     *          storeId: 4,
     *          inTemplate: false
     *          }
     *
     */
    function saveNewWorkRole() {
        var requestJSON = {roleName: "Fryer", roleComment: "Fry meat", storeName: $("#storeName").val(), inTemplate: false};  //TODO: change request values
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "${pageContext.request.contextPath}/store-manager/roster/work-role-save",
            data: JSON.stringify(requestJSON),
            dataType: 'json',
            timeOut: 10000,
            beforeSend: function(request) {  // append csrf token to request header
                request.setRequestHeader(csrf_header, csrf_token);  // header and token must be declared at the header and the top of the script
            },

            success: function (data) {
                console.log("ajax succeeded");
                console.log(data);
            },
            error: function (e) {
                console.log("ajax failed");
                console.log(e);
            },
            done: function (e) {
                console.log("ajax finished");
                console.log(e);
            }

        });
    }


</script>
</html>

