// Get Current Date
var dateDisplay = document.getElementById("dateDisplay")
var weekdaysList = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();
var weekday = today.getDay();

if(dd<10) {
    dd = '0'+dd
} 

if(mm<10) {
    mm = '0'+mm
} 

today = weekdaysList[weekday] + ", " + dd + '/' + mm + '/' + yyyy;
dateDisplay.innerHTML = today;


// MDL Stuff
// Modal dialogue in index file, for pop up window purposes

var dialog = document.querySelector('dialog');
var showDialogButton = document.querySelector('#show-dialog');
if (! dialog.showModal) {
    dialogPolyfill.registerDialog(dialog);
}

showDialogButton.addEventListener('click', function() {
    dialog.showModal();
});

dialog.querySelector('.close').addEventListener('click', function() {
    dialog.close();
});

