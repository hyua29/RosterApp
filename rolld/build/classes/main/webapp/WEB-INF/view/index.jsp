
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="input" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
        <title>Roster Application</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/material.min.css">
        <script src="${pageContext.request.contextPath}/resources/js/material.min.js"></script>
        <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Arimo"> <!-- Change HTML Font -->

        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="mobile-web-app-capable" content="yes" />
        <link rel="shortcut icon" sizes="196x196" href="${pageContext.request.contextPath}/resources/img/logo.png" />
        <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/resources/img/logo.png" />
    
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/index.css">
    
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<body id="body">
    <!-- Simple header with scrollable tabs. -->
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
      <header class="mdl-layout__header">
          
        <div class="mdl-layout__header-row">
          <!-- Title -->
          <span class="mdl-layout-title title" >ROLL'D ROSTER APP</span>
          
        </div>

        <!-- Tabs -->
        <div class="mdl-layout__tab-bar mdl-js-ripple-effect">
            
          <a href="#scroll-tab-1" class="mdl-layout__tab is-active">Monday</a>
          <a href="#scroll-tab-2" class="mdl-layout__tab">Tuesday</a>
          <a href="#scroll-tab-3" class="mdl-layout__tab">Wednesday</a>
          <a href="#scroll-tab-4" class="mdl-layout__tab">Thursday</a>
          <a href="#scroll-tab-5" class="mdl-layout__tab">Friday</a>
          <a href="#scroll-tab-6" class="mdl-layout__tab">Saturday</a>
          <a href="#scroll-tab-7" class="mdl-layout__tab">Sunday</a>
        </div>
      </header>
        
      <!-- Drawer / Left Pane -->
      <div class="mdl-layout__drawer">
        <span class="mdl-layout-title"><img src="${pageContext.request.contextPath}/resources/img/rolld_logo.png" width="130px">Administrator <p id="dateDisplay"></p></span>
        <nav class="mdl-navigation">
            <a class="mdl-navigation__link" href=""><i class="material-icons">schedule</i> Roster</a>
            <a class="mdl-navigation__link" href=""><i class="material-icons">group</i> Staffs</a>
            <a class="mdl-navigation__link" href=""><i class="material-icons">feedback</i> Extra</a>
        </nav>
      </div>

      <!-- Content of the selected page, might change it to dynamic version -->
      <main class="mdl-layout__content">
        <section class="mdl-layout__tab-panel is-active" id="scroll-tab-1">
          <div class="page-content">
              <!-- Your content goes here -->
              
               <!-- Button Sample --> 
                  <button id="show-dialog" type="button" class="mdl-button">CLICK HERE</button>
                  <dialog class="mdl-dialog">
                    <h4 class="mdl-dialog__title">Allow data collection?</h4>
                    <div class="mdl-dialog__content">
                      <p>
                        Allowing us to collect data will let us get you the information you want faster.
                      </p>
                    </div>
                    <div class="mdl-dialog__actions">
                      <button type="button" class="mdl-button">Agree</button>
                      <button type="button" class="mdl-button close">Disagree</button>
                    </div>
                  </dialog>           
                <!-- Dialog Button sample ends here -->
              
          </div>
        </section>
        <section class="mdl-layout__tab-panel" id="scroll-tab-2">
          <div class="page-content"><!-- Your content goes here --></div>
        </section>
        <section class="mdl-layout__tab-panel" id="scroll-tab-3">
          <div class="page-content"><!-- Your content goes here --></div>
        </section>
        <section class="mdl-layout__tab-panel" id="scroll-tab-4">
          <div class="page-content"><!-- Your content goes here --></div>
        </section>
        <section class="mdl-layout__tab-panel" id="scroll-tab-5">
          <div class="page-content"><!-- Your content goes here --></div>
        </section>
        <section class="mdl-layout__tab-panel" id="scroll-tab-6">
          <div class="page-content"><!-- Your content goes here --></div>
        </section>
        <section class="mdl-layout__tab-panel" id="scroll-tab-7">
          <div class="page-content"><!-- Your content goes here --></div>
        </section>
      </main>
        <footer> Prototyped by &copy; GCR, 2018</footer>   
    </div>

</body>
    <script src= "${pageContext.request.contextPath}/resources/js/index.js"></script>
    

       
</html>

