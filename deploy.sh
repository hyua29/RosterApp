#!/bin/sh
#!/bin/bash

account_name='ubuntu' #Account name of the server
target='13.54.162.209' #Address of the target server
key='/home/cooper/Desktop/gcr.pem' #Private key file to log into the server
branch='master' #Branch from which the source code is downloaded
host='gitlab.com/hyua29/RosterApp.git' #Git host
username='username' #Git username
password='password' #Git password
password_ssh='' #Git ssh key
project_name='rolld'
pom_path=$project_name
webapp_path='/var/lib/tomcat8/webapps'
tomcat_bin_path='/usr/share/tomcat8/bin'

while true; do 

echo "What is the git host? (d for default $host)" ## read git host
read temp
if [ "$temp" != "d" ];
	then 
		$host = $temp
fi;
	
echo "What is your git user name? (d for default $username)" # read git user name
read temp
if [ "$temp" != "d" ];
	then 
		$username = $temp
fi;


echo -n "Do you wish to use ssh to clone git repository? (y/n/e)"
read is_ssh

case $is_ssh in
        [Yy]* ) # use ssh to pull
		echo "please specify the path of the private key file (d for default)"; 
		read temp;
		if [ "$temp" != "d" ];
	    	    then 
			$password_ssh = $temp
		fi;
		
		if [ -f "$password_ssh" ];
		then 
		     echo "file found";
		else 
		     echo "file not found";
		     exit;
		fi
		;;
        [Nn]* ) # use password to pull
		echo "please type in your password (d for default)"; 
		read temp;
		if [ "$temp" != "d" ];
	    	then 
				$password = $temp;
	
		fi
		echo "password received";;
		[Ee]* ) exit;;
        * ) echo "Please answer yes or no.";
			exit;;
esac	
	echo "Which branch would you like to deploy? (d for default $branch)";  # read in branch from which the source code is downloaded
	read temp;
	if [ "$temp" != "d" ];
	    then $branch = $temp;
	fi
	
	echo "What is the remote server account name? (d for default $account_name)";  # read in remote server account name
	read temp;
	if [ "$temp" != "d" ];
	    then $account_name = $temp;
	fi

	echo "What is the remote server address? (d for default $target)";  # read in remote server address
	read temp;
	if [ "$temp" != "d" ];
	    then $target = $temp;
	fi
	
	echo "Where is the private key file to log into the server? (d for default $key)";  # private key file to log into the server via ssh 
	read temp;
	if [ "$temp" != "d" ] && [ -f "$temp" ]; then 
		$key = $temp;
		echo "file found";
	elif [ "$temp" != "d" ] && ![ -f "$temp" ]; then
		echo "file not found"; 
		exit;
	fi
	
	echo "Where is the pom.xml file under the branch? (d for default $pom_path)";  # read in remote server address
	read temp;
	if [ "$temp" != "d" ];
	    then $pom_path = $temp;
	fi
	echo "Path is: $branch/$pom_path";

	git_pull_command="sudo rm -fr $branch; 
					  git clone 'https://$username:$password@$host' $branch ;"

	mvn_install_command="";
	echo "Would you like to skip the testing? (y/n)";
	read skip_test;
	case $skip_test in
		[Yy]* ) mvn_install_command="mvn install -f $branch/$pom_path/ -Dmaven.test.skip=true;";;	# skip the tests
		[Nn]* ) mvn_install_command="mvn install -f $branch/$pom_path/; ";;
		* ) exit;;
	esac	

	tomcat_init_command="sudo rm -f $webapp_path/$branch.war;
						sudo mv $branch/$pom_path/target/*.war $webapp_path/$branch.war;"
					#	sudo $tomcat_bin_path/shutdown.sh;
					#	sudo $tomcat_bin_path/startup.sh;"
	ssh "$account_name@$target" -i "$key" "$git_pull_command $mvn_install_command $tomcat_init_command"; 

	echo "Congratulation, the program has been successfully deployed";
	echo "Server address: $target:8080/$branch"
	
	echo "Press 'Enter' to continue";
	read temp;
	clear;

done

