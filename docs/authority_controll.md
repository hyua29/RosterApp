Authority Control
==================

| Roles         | Url                       | View person schedule |  View Roster |  Edit Shift  | Edit Roster  | Role Edit roster template | Invite new Employee | Invite new Manager | Modify Employee Pay Rate Relevant Details | Modify Manager Pay Rate Relevant Details  | Change Pay Rate |
| ------------- |:-------------------------:|:--------------------:|:------------:|:------------:|:------------:|:-------------------------:|:-------------------:|:------------------:|:-----------------------------------------:|:-----------------------------------------:|:---------------:|
| Employee      | /home/*                   |           +          |      +       |      -       |       -      |              -            |           -         |          -         |                      -                    |                    -                      |        -        |
| Manager       | /home/*  /manager/*       |           +          |      +       |      +       |       +      |              +            |           +         |          -         |                      +                    |                    -                      |        -        |
| Owner         | /home/*  /owner/*         |           +          |      +       |      +       |       +      |              +            |           +         |          +         |                      +                    |                    +                      |        +        |
