Time log
========


Janzen
---------
Available Time: 1h

| time | note |
|:-----|:----:|
|   11/07/18 1pm 1hr   |   DB Testing   |
|   12/07/18 2.30am 1hr   |   DB Testing   |


gloria
---------
Available Time: 1h

| time | note |
|:-----|:----:|
|      |      |

Riordan
---------
Available Time: 10h

| time | note |
|:-----|:----:|
|  08/07/18  2hrs  |      |
|  09/07/18  1hr   |      |
|  11/07/18  3hrs  | work on interfaces |
|  12/07/18  3hrs  | add Availabilities page  |


Haoming
---------
Available Time: 25h

| time | note |
|:-----|:----:|
| 2h     |1. fixed broken/failed test; 2. enable auto-testing on push  |



Rico
---------
Available Time: 1h

| time | note |
|:-----|:----:|
| 11/07/18 3.5hrs     |   Fixed the active class in navigation panel, routing and href   |


