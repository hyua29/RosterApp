Time log
========


Janzen
---------
Available Time: 1h

| time | note |
|:-----|:----:|
|   11/07/18 1pm 1hr   |   DB Testing   |
|   12/07/18 2.30am 1hr   |   DB Testing   |


gloria
---------
Available Time: 1h

| time | note |
|:-----|:----:|
|      |      |

Riordan
---------
Available Time: 10h

| time | note |
|:-----|:----:|
|  08/07/18  2hrs  |      |
|  09/07/18  1hr   |      |
|  11/07/18  3hrs  | work on interfaces |
|  12/07/18  3hrs  | add Availabilities page  |


Haoming
---------
Available Time: 20h

| time | note |
|:-----|:----:|
| 1h     | research in testing at dao, service and controller layers     |
| 1h | spike for aop |
| 1.5h | research in mock testing framework |
| 3h   | 1. added ExceptionFactory 2. exception will be logged through ExceptionFactory 3. adapt ExceptionFactory to the code 4. manually tested reset password|
| 1h   |1. changed URL of myScedule 2. used modelAttribute to retrieved object from front-end |
| 3.5h | imported mockmvc and mockito; mockup spike |
| 2h   |1. reset token will be expired after the password has been successfully reset 2. invitation will be expired after the new user has been successfully signed up 3. fixed signup url error |
| 4h  |1. restored missing files and resolved conflicts; 2. helped gloria with ajax; |



Rico
---------
Available Time: 1h

| time | note |
|:-----|:----:|
| 11/07/18 3.5hrs     |   Fixed the active class in navigation panel, routing and href   |


